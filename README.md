# Kirin UI
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=moon-kirin_front&metric=alert_status) ![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=moon-kirin_front&metric=sqale_rating) ![Coverage](https://sonarcloud.io/api/project_badges/measure?project=moon-kirin_front&metric=coverage)](https://sonarcloud.io/summary/new_code?id=moon-kirin_front)

[![Docker Image Version (latest semver)](https://img.shields.io/docker/v/reiizumi/kirin-ui) ![Docker Image Size (latest semver)](https://img.shields.io/docker/image-size/reiizumi/kirin-ui) ![Docker Pulls](https://img.shields.io/docker/pulls/reiizumi/kirin-ui)](https://hub.docker.com/r/reiizumi/kirin-ui)

## Start locally
Because this service is a UI, it requires access to the Kirin backend (and its dependencies) and a Keycloak configured for it.

### OpenAPI
Regenerate the apis and models according to the Kirin OpenAPI:
```
openapi-generator-cli.cmd generate -g typescript-fetch -i https://gitlab.com/moon-kirin/openapi/-/raw/master/reference/Kirin.yaml?ref_type=heads
```

### Running the application in dev mode
To make code changes, start the code in development mode:
```
npm run dev
```

### Container
The service has been created to be packaged in a container with minimal libraries.

```shell script
docker build -t moon-cat/kirin-ui .
```

## Helm
Helm chart is available in [Artifact Hub](https://artifacthub.io/packages/helm/kirin/kirin)

## Quality Check
The code is analyzed according to [SonarCloud](https://sonarcloud.io/project/overview?id=moon-kirin_front)