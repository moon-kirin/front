import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import { AuthProvider, TAuthConfig } from 'react-oauth2-code-pkce'
import Login from './components/Login';

const authConfig: TAuthConfig = {
  clientId: 'kirin',
  authorizationEndpoint: import.meta.env.VITE_AUTH_ENDPOINT,
  tokenEndpoint: import.meta.env.VITE_TOKEN_ENDPOINT,
  logoutEndpoint: import.meta.env.VITE_LOGOUT_ENDPOINT,
  redirectUri: import.meta.env.VITE_REDIRECT_URI,
  scope: 'openid',
};

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <AuthProvider authConfig={authConfig}>
      <Login basePath={import.meta.env.VITE_BASE_PATH} />
    </AuthProvider>
  </React.StrictMode>,
)
