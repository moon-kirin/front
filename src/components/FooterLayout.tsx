import { Layout } from 'antd';
const { Footer } = Layout;

function FooterLayout() {
    return (
      <Footer style={{ textAlign: 'center' }}>
        <a href="https://gitlab.com/moon-kirin/front/-/raw/main/LICENSE?ref_type=heads" target="_blank">MIT License</a> © {new Date().getFullYear()} - Kirin - Created by <a href="https://www.moon.cat/" target="_blank">Moon.cat</a>
      </Footer>
    );
}

export default FooterLayout;