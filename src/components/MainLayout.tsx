import React, { useContext, useEffect } from 'react';
import { TTokenData } from 'react-oauth2-code-pkce/dist/types';
import { BrowserRouter } from 'react-router-dom';
import { Breadcrumb, Layout, theme } from 'antd';
import { BudgetsApi, CategoriesApi, FixedExpensesApi, PayrollApi, ResponseError, SavingApi } from '../kirin';
import { KirinContext } from '../models/KirinContext';
import LeftLayout from './LeftLayout';
import FooterLayout from './FooterLayout';
import { YearUtil } from '../utils/YearUtil';
import { AppContext } from '../models/AppContext';
import { RouteUtil } from '../utils/RouteUtil';

const { Content } = Layout;

interface Props {
    logout: () => void;
    tokenData: TTokenData;
}

const MainLayout: React.FC<Props> = ({ logout, tokenData }) => {
  const appCtx = useContext(AppContext);
  const kirinCtx = useContext(KirinContext);

  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();
  
  const payrollApi = new PayrollApi(appCtx.configuration);
  const budgetApi = new BudgetsApi(appCtx.configuration);
  const savingApi = new SavingApi(appCtx.configuration);
  const categoriesApi = new CategoriesApi(appCtx.configuration);
  const fixedExpensesApi = new FixedExpensesApi(appCtx.configuration);

  //Retrieve the years to check connectivity and user permissions
  useEffect(() => {
    if(kirinCtx.reloadListYears) {
      kirinCtx.setReloadListYears(false);
      if(appCtx.debug) console.log("Loading years...")
      payrollApi.getV1Payrolls().then((reqYears) => {
        if(appCtx.debug) console.log("Years loaded",reqYears);

        if(reqYears.length === 0) {
          if(appCtx.debug) console.log("No years available");
          appCtx.setStatus('empty');
        } else {
          if(appCtx.debug) console.log("Years available");
          
          //Sort years and set the list
          const locyears = YearUtil.transform(reqYears); 
          kirinCtx.setListYears(locyears);

          //Set the current year and save it in local storage
          kirinCtx.changeCurrentYear(YearUtil.getCurrentYear(appCtx.debug, locyears));

          appCtx.setStatus('ok');
        }
      }).catch((error: ResponseError) => {
        if(appCtx.debug) console.log("Error loading years",error);
        
        if(error?.response?.status === 403) {
          console.error("User doesn't have permissions to use Kirin");
          appCtx.setStatus('forbidden');
        } else {
          appCtx.setStatus('error');
        }
      });
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [kirinCtx.reloadListYears]);

  //When the year is changed, load the info from that year
  useEffect(() => {
    if(kirinCtx.currentYear !== 0) {
      if(appCtx.debug) console.log("Current year modified", kirinCtx.currentYear);

      //Load the payroll
      payrollApi.getV1PayrollsYear({year: kirinCtx.currentYear}).then((payroll) => {
        if(appCtx.debug) console.log("Payroll loaded",payroll);
        kirinCtx.setPayroll(payroll);
      }).catch((error: ResponseError) => {
        if(appCtx.debug) console.log("Error loading payroll",error);
        appCtx.setStatus('error');
      });

      kirinCtx.setRefreshBudgets(true);
      kirinCtx.setRefreshSavings(true);
      kirinCtx.setRefreshFixedExpenses(true);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [kirinCtx.currentYear]);

  useEffect(() => {
    if(kirinCtx.refreshBudgets) {
      kirinCtx.setRefreshBudgets(false);

      budgetApi.getV1Budgets({year: kirinCtx.currentYear}).then((budgets) => {
        if(appCtx.debug) console.log("Budgets loaded",budgets);
        kirinCtx.setBudgets(budgets);
      }).catch((error: ResponseError) => {
        if(appCtx.debug) console.log("Error loading budgets",error);
        appCtx.setStatus('error');
      });
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [kirinCtx.refreshBudgets]);

  useEffect(() => {
    if(kirinCtx.refreshSavings) {
      kirinCtx.setRefreshSavings(false);

      savingApi.getV1Savings({year: kirinCtx.currentYear}).then((savings) => {
        if(appCtx.debug) console.log("Savings loaded",savings);
        kirinCtx.setSavings(savings);
      }).catch((error: ResponseError) => {
        if(appCtx.debug) console.log("Error loading savings",error);
        appCtx.setStatus('error');
      });
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [kirinCtx.refreshSavings]);

  useEffect(() => {
    if(kirinCtx.refreshFixedExpenses) {
      kirinCtx.setRefreshFixedExpenses(false);

      fixedExpensesApi.getV1FixedExpenses({year: kirinCtx.currentYear}).then((fixedExpenses) => {
        if(appCtx.debug) console.log("Fixed expenses loaded",fixedExpenses);
        kirinCtx.setFixedExpenses(fixedExpenses);
      }).catch((error: ResponseError) => {
        if(appCtx.debug) console.log("Error loading fixed expenses",error);
        appCtx.setStatus('error');
      });
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [kirinCtx.refreshFixedExpenses]);

  useEffect(() => {
    categoriesApi.getApiV1Categories().then((categories) => {
      if(appCtx.debug) console.log("Categories loaded",categories);
      kirinCtx.setCategories(categories);
    }).catch((error: ResponseError) => {
      if(appCtx.debug) console.log("Error loading categories",error);
      appCtx.setStatus('error');
    });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);


  const content = RouteUtil.getRoutes(appCtx.status);
  
  return (
    <BrowserRouter>
      <Layout style={{ minHeight: '100vh' }}>
        <LeftLayout logout={logout} tokenData={tokenData} /> 
        <Layout>
          <Content style={{ margin: '0 16px' }}>
            <Breadcrumb items={appCtx.breadcrumb}
              style={{ margin: '16px 0' }}
            />
            <div className="base-container"
              style={{
                background: colorBgContainer,
                borderRadius: borderRadiusLG,
              }}
            >
              {content}
            </div>
          </Content>
          <FooterLayout />
        </Layout>
      </Layout>
    </BrowserRouter>
  );
};

export default MainLayout;