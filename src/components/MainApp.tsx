import { ItemType } from 'antd/es/breadcrumb/Breadcrumb';
import { OptionProps } from 'antd/es/select';
import React, { useMemo, useState } from 'react';
import { Budget, Category, Configuration, FixedExpense, Payroll, Saving } from '../kirin';
import { TTokenData } from 'react-oauth2-code-pkce/dist/types';
import { AppContext } from '../models/AppContext';
import { KirinContext } from '../models/KirinContext';
import MainLayout from './MainLayout';

interface Props {
    logout: () => void;
    tokenData: TTokenData;
    configuration: Configuration;
}

const MainApp: React.FC<Props> = ({ logout, tokenData, configuration }) => {
    const debug = import.meta.env.VITE_CONF_LOG;

    // App definition
    const [status, setStatus] = useState<string>('initial');
    const [breadcrumb, setBreadcrumb] = useState<ItemType[]>();

    // Kirin definition
    const [currentYear, setCurrentYear] = useState<number>(0);
    const [listYears, setListYears] = useState<OptionProps[]>([]);
    const [reloadListYears, setReloadListYears] = useState<boolean>(true);
    const changeCurrentYear = (newCurrentYear: number) => {
        localStorage.setItem('selectedYear', newCurrentYear.toString());
        setCurrentYear(newCurrentYear);
    };
    const [payroll, setPayroll] = useState<Payroll>();
    const [budgets, setBudgets] = useState<Budget[]>([]);
    const [refreshBudgets, setRefreshBudgets] = useState<boolean>(false);
    const [savings, setSavings] = useState<Saving[]>([]);
    const [refreshSavings, setRefreshSavings] = useState<boolean>(false);
    const [fixedExpenses, setFixedExpenses] = useState<FixedExpense[]>([]);
    const [refreshFixedExpenses, setRefreshFixedExpenses] = useState<boolean>(false);
    const [categories, setCategories] = useState<Category[]>([]);

    const appContextValue = useMemo(() => ({ 
        debug, configuration,
        status, setStatus, 
        breadcrumb, setBreadcrumb 
    }), [status, configuration, breadcrumb]);
    const kirinContextValue = useMemo(() => ({ 
        currentYear, changeCurrentYear, 
        listYears, setListYears, 
        reloadListYears, setReloadListYears,
        payroll, setPayroll,
        budgets, setBudgets, 
        refreshBudgets, setRefreshBudgets,
        savings, setSavings, 
        refreshSavings, setRefreshSavings,
        categories, setCategories,
        fixedExpenses, setFixedExpenses, 
        refreshFixedExpenses, setRefreshFixedExpenses
    }), [currentYear, listYears, payroll, budgets, savings, fixedExpenses, refreshBudgets, refreshSavings, refreshFixedExpenses]);

    return (
       <AppContext.Provider value={appContextValue}>
            <KirinContext.Provider value={kirinContextValue}>
                <MainLayout logout={logout} tokenData={tokenData} />
            </KirinContext.Provider>
        </AppContext.Provider>
    );
};

export default MainApp;