import { Form, Input, Select } from 'antd';
import TextArea from 'antd/es/input/TextArea';
import React, { useContext } from 'react';
import { KirinContext } from '../../models/KirinContext';
import { CategoryUtil } from '../../utils/CategoryUtil';

const { Option } = Select;
const { render } = CategoryUtil();

const FormCommons: React.FC = () => {
    const kirinCtx = useContext(KirinContext);

    return (
        <>
            <Form.Item name="id" style={{ display: 'none' }}>
                <Input type="hidden" />
            </Form.Item>
            <Form.Item name="name" label="Name" rules={[{ required: true }]}>
                <Input />
            </Form.Item>
            <Form.Item name="description" label="Description">
                <TextArea />
            </Form.Item>
            <Form.Item name="categoryId" label="Category" rules={[{ required: true }]}>
                <Select>
                    {kirinCtx.categories.map(category => (
                        <Option value={category.id} key={category.id}>
                            {render(category.id, kirinCtx.categories)}
                        </Option>
                    ))}
                </Select>
            </Form.Item>
        </>
    );
};

export default FormCommons;