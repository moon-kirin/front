import React, { useContext, useState } from 'react';
import { Alert, Button, Form, Input, Modal, Typography } from 'antd';
import { AppContext } from '../../models/AppContext';
import { KirinContext } from '../../models/KirinContext';
import { PayrollApi } from '../../kirin';
import { PlusOutlined } from '@ant-design/icons';

const { Paragraph } = Typography;

const AddYear: React.FC = () => {
    const appCtx = useContext(AppContext);
    const kirinCtx = useContext(KirinContext);

    const api = new PayrollApi(appCtx.configuration);

    const [form] = Form.useForm();
    const [openYear, setOpenYear] = useState(false);
    const [errorMessage, setErrorMessage] = useState<string>();

    const showAddYearModal = () => {
        setErrorMessage(undefined);
        form.resetFields();
        setOpenYear(true);
        
    };
    const handleCancel = () => {
        setOpenYear(false);
    };

    const handleOk = () => {
        setErrorMessage(undefined);
        form.validateFields()
            .then((values) => {
                if(appCtx.debug) console.log("Form values:", values);
                const yearNum = Number(values.year);
                if (kirinCtx.listYears.some((year) => year.value === yearNum)) {
                    if (appCtx.debug) console.log("Year already exists");
                    setErrorMessage("The chosen year exists.");
                } else {
                    if (appCtx.debug) console.log("Year does not exist");
                    api.postV1PayrollsYear({ year: values.year }).then(() => {
                        if (appCtx.debug) console.log("Year created");
                        kirinCtx.setReloadListYears(true);
                        setOpenYear(false);
                    })
                    .catch((error) => {
                        console.log("Form validation error:", error);
                        setErrorMessage("The new year could not be created, check the logs.");
                    });
                }
            })
            .catch((error) => {
                console.log("Form validation error:", error);
            });
    };
    
    return (
        <>
            <div className="addYear">
                <Button icon={<PlusOutlined />} onClick={showAddYearModal}>Year</Button>
            </div>
            <Modal
                open={openYear}
                title="Add new year"
                onOk={handleOk}
                onCancel={handleCancel}
                footer={[
                <Button key="back" onClick={handleCancel}>
                    Cancel
                </Button>,
                <Button key="submit" type="primary" onClick={handleOk}>
                    Create
                </Button>,
                ]}
            >
                <Form
                    form={form}
                    name="basic"
                    wrapperCol={{ span: 15 }}
                >
                    {errorMessage && (
                        <Alert
                            type="error"
                            message={errorMessage}
                            showIcon
                            closable
                        />
                    )}
                    {appCtx.status === 'empty' ? (
                        <Paragraph>
                            I see it's your first time here, so this is going to be simple: indicate the year you want to use to start using Kirin, 
                            for example, the current one.
                        </Paragraph>
                    ) : (
                        <Paragraph>
                            It's time to define a new year to start managing it! Remember to put a new one, the existing ones do not need to be created.
                        </Paragraph>
                    )}
                    <Form.Item label="Year" name="year" rules={
                        [{
                            required: true, 
                            message: 'You must indicate a valid year (min year 2000)', 
                            type: 'number', 
                            min: 2000, 
                            transform(value) {
                                return Number(value)
                            }, 
                        }]}>
                        <Input type='number' />
                    </Form.Item>
                </Form>
            </Modal>
            
        </>
    );
};

export default AddYear;