import React from 'react';
import { Collapse } from 'antd';
import Table, { ColumnsType } from 'antd/es/table';
import { Purchase } from '../../kirin';
import { monthNames } from '../constants/constants';

interface PurchaseMonthProps {
  purchases: Purchase[];
  columns: ColumnsType<Purchase>;
  monthly: boolean
}

interface ItemType {
    key: number;
    label: string;
    children: JSX.Element;
}

const PurchaseMonth: React.FC<PurchaseMonthProps> = ({ purchases, columns, monthly }) => {
    const currentMonth = new Date().getMonth() + 1;

    const panels: ItemType[] = monthly ? monthNames.map((name, index) => {
        const monthPurchases = purchases.filter(purchase => purchase.month === index + 1);
        return monthPurchases.length > 0 ? {
            key: index + 1,
            label: name,
            children: <Table columns={columns} dataSource={monthPurchases} rowKey="id" />
        } : null;
    }).filter(Boolean) : [];
    
    return (
        <>
            {monthly ? (
                <Collapse items={panels} defaultActiveKey={[currentMonth]} style={{ margin: '15px 0' }} />
            ) : (
                <Table columns={columns} dataSource={purchases} rowKey="id" />
            )}
        </>
    );
};

export default PurchaseMonth;