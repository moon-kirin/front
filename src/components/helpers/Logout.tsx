import React from 'react';
import { Button } from 'antd';
import { LogoutOutlined } from '@ant-design/icons';

type LogoutProps = {
    logout: () => void;
};

const Logout: React.FC<LogoutProps> = ({ logout }) => {
    return (
       <div className="signout">
            <Button type="primary" danger icon={<LogoutOutlined />} onClick={() => logout()}>
                Sign out
            </Button>
        </div>
    );
};

export default Logout;