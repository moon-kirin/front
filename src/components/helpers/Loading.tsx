import React from 'react';
import { Spin } from 'antd';

const Loading: React.FC = () => {
    return (
        <div style={{ display: 'flex', justifyContent: 'center', height: '100vh' }}>
            <Spin size="large" />
        </div>
    );
};

export default Loading;