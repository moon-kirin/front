import React, { useContext } from 'react';
import { Typography, Table, notification, Checkbox } from 'antd';
import { AppContext } from '../../models/AppContext';
import { KirinContext } from '../../models/KirinContext';
import { FixedExpensesApi } from '../../kirin';
import { monthNames } from '../constants/constants';

const { Paragraph } = Typography;

const MarkedExpenses: React.FC = () => {
    const appCtx = useContext(AppContext);
    const kirinCtx = useContext(KirinContext);
    
    const api = new FixedExpensesApi(appCtx.configuration);
    const currentMonth = monthNames[new Date().getMonth()];

    const dataSource = [...monthNames, 'Annual'].map((month, index) => {
        const expenses = kirinCtx.fixedExpenses
            .filter(expense => expense.months?.find(m => m.month === (index < 12 ? index + 1 : 0)) && expense.requireMark)
            .map(expense => {
                const monthObject = expense.months?.find(m => m.month === (index < 12 ? index + 1 : 0));
                return {
                    id: expense.id,
                    name: expense.name,
                    paid: monthObject?.paid || false,
                    month: monthObject?.month || 0
                };
            });

        return {
            key: index,
            month,
            expenses
        };
    }).filter(row => row.expenses.length > 0);

    const columns = [
        {
            title: 'Month',
            dataIndex: 'month',
            key: 'month',
        },
        {
            title: 'Expenses',
            dataIndex: 'expenses',
            key: 'expenses',
            render: expenses => expenses.map((expense, index: number) => (
                <div key={expense.id}>
                    <Checkbox 
                        style={{ marginRight: '5px' }}
                        checked={expense.paid} 
                        onChange={(e) => handleCheckboxChange(expense.id, expense.month, e.target.checked)}
                    />
                    {expense.name}
                </div>
            ))
        },
    ];

    const handleCheckboxChange = (expenseId: number, month: number, newValue: boolean) => {
        if(appCtx.debug) console.log(expenseId, month, newValue); 

        const expense = kirinCtx.fixedExpenses.find(expense => expense.id === expenseId);
        if (expense) {
            const monthObject = expense.months.find(m => m.month === month);
            if (monthObject) {
                monthObject.paid = newValue;


                api.putV1FixedExpensesId({id: expenseId, fixedExpense: expense}).then(() => {
                    if(appCtx.debug) console.log("Fixed Expense updated");
                    kirinCtx.setRefreshFixedExpenses(true);

                    notification.success({message: 'Mark updated',});
                })
                .catch(error => {
                    console.error("error", error);
                    notification.error({
                        message: 'Error updating the fixed expense',
                        description: 'There was an error updating the fixed expense. Check the logs.',
                    });
                });
            }
        }
    };

    return (
        <div>
            <Paragraph>
                Check if you have paid this month's expenses.
            </Paragraph>
            <Table 
                dataSource={dataSource} 
                columns={columns} 
                pagination={false} 
                rowClassName={(record) => record.month === currentMonth ? 'current-month' : ''}
            />
        </div>
    );
};

export default MarkedExpenses;