import React from 'react';
import { Divider, Typography } from 'antd';
import { Budget, Purchase } from '../../kirin';
import PurchaseSummary from './PurchaseSummary';

interface PurchasesChartsProps {
    purchases: { [key: number]: Purchase[] };
    budgets: Budget[];
}

const { Paragraph } = Typography;

const PurchasesCharts: React.FC<PurchasesChartsProps> = ({ purchases, budgets }) => {
    console.log("budgets", budgets)
    console.log("purchases", purchases);
    return (
        <>
            {budgets.map(budget => 
                <div key={budget.id}>
                    <Divider orientation="left">{budget.name}</Divider>
                        
                    {purchases[budget.id] && purchases[budget.id].length > 0 ? (
                        <PurchaseSummary 
                            purchases={purchases[budget.id]} 
                            monthly={budget.type === 'MONTHLY'} 
                            budget={budget} 
                        />
                    ) : (
                        <Paragraph>There are no purchases assigned to this budget.</Paragraph>
                    )}
                </div>
            )}
        </> 
    );
};

export default PurchasesCharts;
