import React from 'react';
import { Avatar } from 'antd';
import { TokenUtil } from '../../utils/TokenUtil';
import { TTokenData } from 'react-oauth2-code-pkce/dist/types';
import { UserOutlined } from '@ant-design/icons';

interface UserLogoProps {
    collapsed: boolean, 
    tokenData: TTokenData;
}

const UserLogo: React.FC<UserLogoProps> = ({ collapsed, tokenData }) => {
    const username = TokenUtil.getUsername(tokenData);
    const initials = TokenUtil.getInitials(username);

    return (
        <div className="user">
            <Avatar icon={<UserOutlined />} />
            {!collapsed ? username : initials}
        </div>
    );
};

export default UserLogo;