import React from 'react';
import logo from '../../assets/logo.png'

const Logo: React.FC = () => {
    return (
        <div className="logo-vertical"><img src={logo} alt="Kirin logo" /></div>
    );
};

export default Logo;