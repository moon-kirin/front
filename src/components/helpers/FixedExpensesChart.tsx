import React, { useContext, useEffect, useState } from 'react';
import { KirinContext } from '../../models/KirinContext';
import { PayrollUtil } from '../../utils/PayrollUtil';
import { FixedExpenseUtil } from '../../utils/FixedExpenseUtil';
import SummaryBasicChart from '../helpers/SummaryBasicChart';

const FixedExpensesChart: React.FC = () => {
    const kirinCtx = useContext(KirinContext);
    
    const [totalPayroll, setTotalPayroll] = useState<number>(0);
    const [totalBudgets, setTotalBudgets] = useState<number>(0);
    const [totalSavings, setTotalSavings] = useState<number>(0);
    const [totalFixedExpenses, setTotalFixedExpenses] = useState<number>(0);

    useEffect(() => {
            if(kirinCtx.payroll) {
                const totalMonths = PayrollUtil.sumMonths(kirinCtx.payroll);
                const totalExtra = PayrollUtil.sumExtra(kirinCtx.payroll);
                setTotalPayroll(totalMonths+totalExtra);

                setTotalBudgets(PayrollUtil.sumBudgets(kirinCtx.budgets));
                setTotalSavings(PayrollUtil.sumSavings(kirinCtx.savings));

                setTotalFixedExpenses(FixedExpenseUtil.sumFixedExpenses(kirinCtx.fixedExpenses));
            }
        }, [kirinCtx.payroll, kirinCtx.budgets, kirinCtx.savings, kirinCtx.fixedExpenses]);

    const data = [
        {
            "category": "Payroll",
            "Budget": totalBudgets,
            "BudgetColor": "hsl(198, 70%, 50%)",
            "Savings": totalSavings,
            "Fixed Expenses": totalFixedExpenses,
            "Unallocated": totalPayroll - totalBudgets - totalSavings - totalFixedExpenses,
        },
    ];

    return (
        <SummaryBasicChart keys={["Budget", "Savings", "Fixed Expenses", "Unallocated"]} data={data} total={totalPayroll} />
    );
};

export default FixedExpensesChart;