import React, { useContext } from 'react';
import Select from 'antd/es/select';
import { KirinContext } from '../../models/KirinContext';
import { AppContext } from '../../models/AppContext';

const YearSelector: React.FC = () => {
    const appCtx = useContext(AppContext);
    const kirinCtx = useContext(KirinContext);

    const changeYear = (value: number) => {
        if(appCtx.debug) console.log("Year changed", value);
        kirinCtx.changeCurrentYear(value);
    };

    return (
        <Select
            className="selectedYear"
            options={kirinCtx.listYears}
            value={kirinCtx.currentYear}
            onChange={changeYear}
        />
    );
};

export default YearSelector;