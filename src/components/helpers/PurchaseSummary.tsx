import React from 'react';
import { Purchase, Budget } from '../../kirin';
import SummaryBasicChart from '../helpers/SummaryBasicChart';
import { monthNames } from '../constants/constants';

interface PurchaseSummaryProps {
    purchases: Purchase[];
    monthly: boolean;
    budget: Budget;
}

const PurchaseSummary: React.FC<PurchaseSummaryProps> = ({ purchases, monthly, budget }) => {

    const getColor = (bar) => {
        const percentage = bar.data.Spent / budget.budget;
        if (percentage > 1) {
            return '#FF3333';
        } else if (percentage <= 1 && percentage > 0.75) {
            return '#FFFF66';
        } else if (percentage < 0.75 && percentage >= 0.5) {
            return '#66CCFF';
        } else {
            return '#99FFCC';
        }
    };

    const data = monthly ? monthNames.map((month, index) => {
        const monthTotal = purchases.filter(purchase => purchase.month === index + 1).reduce((total, purchase) => total + purchase.price, 0);
        return {
            category: monthNames[index],
            Spent: monthTotal,
        };
    }) : [{
        category: budget.name,
        Spent: purchases.reduce((total, purchase) => total + purchase.price, 0),
    }];

    return <SummaryBasicChart keys={["Spent"]} data={data} total={budget.budget} showLegends={false} colors={getColor} />;
};

export default PurchaseSummary;