import React from 'react';
import { BarDatum, ComputedDatum, ResponsiveBar } from '@nivo/bar';
import { OrdinalColorScaleConfig } from '@nivo/colors';

interface SummaryBasicChartProps {
    keys: string[];
    data: BarDatum[];
    total: number;
    showLegends?: boolean;
    colors?: OrdinalColorScaleConfig<ComputedDatum<BarDatum>>;
}

const SummaryBasicChart: React.FC<SummaryBasicChartProps> = ({ keys, data, total, showLegends = true, colors }) => {
    const height = data.length > 5 ? '250px' : '150px';
    return (
        <div style={{height}}>
          <ResponsiveBar
            data={data.slice().reverse()}
            keys={keys}
            indexBy="category"
            layout="horizontal"
            margin={{ right: 130, bottom: 20, left: 60 }}
            valueScale={{ type: 'linear', max: total }}
            colors={colors || { scheme: 'nivo' }}
            animate={true}
            legends={showLegends ? [
              {
                dataFrom: 'keys',
                anchor: 'bottom-right',
                direction: 'column',
                justify: false,
                translateX: 120,
                translateY: 0,
                itemsSpacing: 2,
                itemWidth: 100,
                itemHeight: 20,
                itemDirection: 'left-to-right',
                itemOpacity: 0.85,
                symbolSize: 20,
                effects: [
                  {
                    on: 'hover',
                    style: {
                        itemOpacity: 1
                    }
                  }
                ]
              }
            ] : []}
          />
        </div>
    );
};

export default SummaryBasicChart;