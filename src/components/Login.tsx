import React, { useContext } from 'react';
import { AuthContext, IAuthContext } from 'react-oauth2-code-pkce';
import MainApp from './MainApp';
import { Configuration } from '../kirin';

interface LoginProps {
    basePath: string;
}

const Login: React.FC<LoginProps> = ({ basePath }) => {
    const { tokenData, logOut, error, token }: IAuthContext = useContext(AuthContext);
    const configuration = new Configuration ({
      basePath: basePath,
      accessToken: "Bearer "+token,
    });

    if (error) {
      return (
        <>
          <div style={{ color: 'red' }}>An error occurred during authentication: {error}</div>
          <button onClick={() => logOut()}>Logout</button>
        </>
      )
    }
    return (
      <>
        {tokenData ? (
          <MainApp logout={logOut} tokenData={tokenData} configuration={configuration} />
        ) : (
        <div>Redirecting ...</div>
        )}
      </>
    )
};
export default Login;
