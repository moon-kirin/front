import React, { useContext, useState } from 'react';
import { Button, Checkbox, Divider, Form, InputNumber, Modal, Select, Table, Typography, notification, Alert } from 'antd';
import { MinusCircleOutlined, EditOutlined, PlusOutlined, CheckOutlined, CloseOutlined } from '@ant-design/icons';
import { AppContext } from '../../models/AppContext';
import { KirinContext } from '../../models/KirinContext';
import { FixedExpense, FixedExpenseTypeEnum, FixedExpensesApi, FixedExpenseMonth } from '../../kirin';
import { CategoryUtil } from '../../utils/CategoryUtil';
import FormCommons from '../helpers/FormCommons';
import { ColumnsType } from 'antd/es/table';

const { render } = CategoryUtil();
const { Paragraph } = Typography;
const { Option } = Select;

const FixedExpensesSubSection: React.FC = () => {
    const appCtx = useContext(AppContext);
    const kirinCtx = useContext(KirinContext);

    const api = new FixedExpensesApi(appCtx.configuration);
    const [fixedExpensesForm] = Form.useForm();

    const [visible, setVisible] = useState(false);

    const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    const columns: ColumnsType = [
        {
            title: 'Name', 
            dataIndex: 'name', 
            key: 'name',
            sorter: (a: FixedExpense, b: FixedExpense) => a.name.localeCompare(b.name),
        },
        { title: 'Description', dataIndex: 'description', key: 'description' },
        { 
            title: 'Category', 
            dataIndex: 'categoryId', 
            key: 'categoryId',
            render: (categoryId: number) => render(categoryId, kirinCtx.categories),
            sorter: (a: FixedExpense, b: FixedExpense) => a.categoryId - b.categoryId,
            defaultSortOrder: 'ascend',
        },
        { 
            title: 'Type', 
            dataIndex: 'type', 
            key: 'type',
            sorter: (a: FixedExpense, b: FixedExpense) => a.type.localeCompare(b.type),
        },
        { 
            title: 'Price', 
            dataIndex: 'price', 
            key: 'price', 
            sorter: (a: FixedExpense, b: FixedExpense) => a.price - b.price,
        },
        { 
            title: 'Require Mark', 
            dataIndex: 'requireMark', 
            key: 'requireMark', 
            render: (requireMark: boolean) => requireMark ? <CheckOutlined /> : <CloseOutlined />
        },
        {
            title: 'Months',
            dataIndex: 'months',
            key: 'months',
            render: (months: FixedExpenseMonth[]) => months.map(monthObj => monthNames[monthObj.month - 1]).join(', ')
        },
        {
            title: 'Action',
            key: 'action',
            render: (text: string, record: FixedExpense) => (
                <>
                    <Button onClick={() => handleEdit(record)} icon={<EditOutlined />} />
                    <Button onClick={() => handleDelete(record)} icon={<MinusCircleOutlined />} />
                </>
            ),
        },
    ];

    const [modalTitle, setModalTitle] = useState("");
    const [modalActionAdd, setModalActionAdd] = useState(true);

    const handleAdd = () => {
        setModalTitle("Create new Fixed Expense");
        setModalActionAdd(true);
        setVisible(true);
        fixedExpensesForm.resetFields();
    };

    const handleEdit = (record: FixedExpense) => {
        setModalTitle(`Edit the Fixed Expense: ${record.name}`);
        setModalActionAdd(false);
        setVisible(true);
        
        // Map the months array of the record to an array of month properties
        const recordWithMappedMonths = { ...record, months: record.months?.map(monthObj => monthObj.month) };
        fixedExpensesForm.setFieldsValue(recordWithMappedMonths);
    };

    const handleDelete = (record: FixedExpense) => {
        // delete record
        api.deleteV1FixedExpensesId({id: record.id}).then(() => {
            if(appCtx.debug) console.log("Fixed Expense deleted");
            kirinCtx.setFixedExpenses(kirinCtx.fixedExpenses.filter(expense => expense.id !== record.id));

            notification.success({message: 'Fixed Expense deleted',});
        })
        .catch(error => {
            console.error("error", error);
            notification.error({
                message: 'Error deleting the fixed expense',
                description: 'There was an error deleting the fixed expense. Check the logs.',
            });
        });
    };
    
    const handleOk = () => {
        fixedExpensesForm.validateFields().then(values => {
            if(appCtx.debug) console.log('Success:', values);
            values.year = kirinCtx.currentYear;

            // Map the months array of the values to an array of objects with month and paid properties
            if (!Array.isArray(values.months)) {
                values.months = [{ month: values.months, paid: false }];
            } else {
                values.months = values.months.map((month: number) => ({ month, paid: false }));
            }
            if(values.type === FixedExpenseTypeEnum.Annual) {
                values.months = [{month: 0, paid: false}];
            }
            
            modalActionAdd ? handleApiAdd(values) : handleApiEdit(values);
        }).catch(info => {
            console.log('Validate Failed:', info);
        });
    };

    const handleApiAdd = (values: FixedExpense) => {  
        api.postV1FixedExpenses({fixedExpense: values}).then(response => {
            if(appCtx.debug) console.log("Fixed Expense created");
            if(values.type === FixedExpenseTypeEnum.Annual) {
                values.months = [];
            }
            kirinCtx.setRefreshFixedExpenses(true);

            notification.success({message: 'Fixed Expense created',});
            setVisible(false);
        })
        .catch(error => {
            console.error("error", error);
            notification.error({
                message: 'Error creating the fixed expense',
                description: 'There was an error creating the fixed expense. Check the logs.',
            });
        });
    };
    const handleApiEdit = (values: FixedExpense) => {
        api.putV1FixedExpensesId({id: values.id, fixedExpense: values}).then(() => {
            if(appCtx.debug) console.log("Fixed Expense updated");
            if(values.type === FixedExpenseTypeEnum.Annual) {
                values.months = [];
            }
            
            const updatedFixedExpenses = kirinCtx.fixedExpenses.map(expense => {
                if (expense.id === values.id) {
                    return values;
                }
                return expense;
            });
            kirinCtx.setFixedExpenses(updatedFixedExpenses);

            notification.success({message: 'Fixed Expense updated',});
            setVisible(false);
        })
        .catch(error => {
            console.error("error", error);
            notification.error({
                message: 'Error updating the fixed expense',
                description: 'There was an error updating the fixed expense. Check the logs.',
            });
        });
    };

    return (
        <>
            <Divider className="header" orientation="center">Fixed Expenses</Divider>
            <Paragraph>
                During the year we are going to have expenses that we can predict and others that we can't. The unpredictable 
                ones are simple: we will assign them to wallets and try to survive them month after month.
            </Paragraph>
            <Paragraph>
                Those that are predictable (mortgages, taxes, maintenance, average cost of electricity, ...) are added in this 
                section to avoid future work.
            </Paragraph>
            <Paragraph>
                Fixed expenses can be of three types:
            </Paragraph>
            <ul>
                <li>
                    <b>Month</b>: Assigned to one or more months. Its cost will be multiplied according to the number of months.
                </li>
                <li>
                    <b>Unitary</b>: It is assigned to a single month.
                </li>
                <li>
                    <b>Annual</b>: Same as the unitary but does not require saying which month it is indicated for. Useful for 
                    those expenses that we know will happen during the year, but we don't know when it will be, or we simply don't care.
                </li>
            </ul>
            <Paragraph>
                If we want, we can assign which ones need to be checked. Every month you will see a list of the expenses you will 
                have and when you check them you will have a track of them. This is useful to remember that this month we will get 
                our taxes, something that will make our wallet fly away. 
            </Paragraph>

            <Table columns={columns} dataSource={kirinCtx.fixedExpenses} rowKey="id" />
            <Button type="dashed" onClick={handleAdd} block icon={<PlusOutlined />}>
                Add
            </Button>
            <Modal title={modalTitle} open={visible} onOk={handleOk} onCancel={() => setVisible(false)}>
                <Form form={fixedExpensesForm} layout="horizontal" initialValues={{ type: FixedExpenseTypeEnum.Unitary }} labelCol={{ span: 6 }}>
                    {!modalActionAdd && fixedExpensesForm.getFieldValue('requireMark') && (
                        <Alert
                            type="warning"
                            message="After editing the Fixed Expense, you will need to re-mark previously marked months"
                            style={{ marginBottom: '20px' }}
                        />
                    )}
                    <FormCommons />
                    <Form.Item name="type" label="Type" rules={[{ required: true }]}>
                        <Select>
                            <Option value={FixedExpenseTypeEnum.Unitary}>Unitary</Option>
                            <Option value={FixedExpenseTypeEnum.Monthly}>Monthly</Option>
                            <Option value={FixedExpenseTypeEnum.Annual}>Annual</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item name="price" label="Price" rules={[{ required: true }]}>
                        <InputNumber />
                    </Form.Item>
                    <Form.Item name="requireMark" label="Require Mark" valuePropName="checked">
                        <Checkbox />
                    </Form.Item>
                    <Form.Item noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.type !== currentValues.type}>
                    
                        {({ getFieldValue }) => {
                            const type = getFieldValue('type');
                            let monthsProps = {};

                            if (type === FixedExpenseTypeEnum.Annual) {
                                monthsProps = { hidden: true };
                            } else if (type === FixedExpenseTypeEnum.Unitary) {
                                monthsProps = {
                                    rules: [
                                        { required: true, message: 'Please select a month' },
                                        {
                                            validator: (_, value) => {
                                                if (value && value.length > 1) {
                                                    return Promise.reject(new Error('Please select only one month'));
                                                }
                                                return Promise.resolve();
                                            },
                                        },
                                    ],
                                };
                            } else if (type === FixedExpenseTypeEnum.Monthly) {
                                monthsProps = {
                                    rules: [{ required: true, message: 'Please select at least one month' }],
                                };
                            }

                            return (
                                <Form.Item 
                                    name="months" 
                                    label="Months" 
                                    {...monthsProps}
                                >
                                    <Select 
                                        mode={type === FixedExpenseTypeEnum.Monthly ? 'multiple' : undefined}
                                        value={fixedExpensesForm.getFieldValue('months') ? fixedExpensesForm.getFieldValue('months').map((monthObj: { month: number, paid: boolean }) => monthObj.month) : []}
                                    >
                                        <Option key={1} value={1}>January</Option>
                                        <Option key={2} value={2}>February</Option>
                                        <Option key={3} value={3}>March</Option>
                                        <Option key={4} value={4}>April</Option>
                                        <Option key={5} value={5}>May</Option>
                                        <Option key={6} value={6}>June</Option>
                                        <Option key={7} value={7}>July</Option>
                                        <Option key={8} value={8}>August</Option>
                                        <Option key={9} value={9}>September</Option>
                                        <Option key={10} value={10}>October</Option>
                                        <Option key={11} value={11}>November</Option>
                                        <Option key={12} value={12}>December</Option>
                                    </Select>
                                </Form.Item>
                            );
                        }}
                    </Form.Item>
                </Form>
            </Modal>
        </>
    );
};

export default FixedExpensesSubSection;