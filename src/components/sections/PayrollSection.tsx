import React, { useContext } from 'react';
import { KirinContext } from '../../models/KirinContext';
import PayrollSubSection from './PayrollSubSection';
import Loading from '../helpers/Loading';
import BudgetSubSection from './BudgetSubSection';
import SavingsSubSection from './SavingsSubSection';
import PayrollSummarySubSection from './PayrollSummarySubSection';

const PayrollSection: React.FC = () => {
    const kirinCtx = useContext(KirinContext);

    if(kirinCtx.payroll === undefined) {
        return <Loading />;
    }
    return (
        <>
            <PayrollSubSection />
            <BudgetSubSection />
            <SavingsSubSection />
            <PayrollSummarySubSection />
        </>
    );
};

export default PayrollSection;