import React, { useContext, useEffect, useState } from 'react';
import { Button, Divider, Form, FormProps, Input, InputNumber, Select, Space, Typography, notification } from 'antd';
import { Budget, BudgetsApi } from '../../kirin';
import { AppContext } from '../../models/AppContext';
import { KirinContext } from '../../models/KirinContext';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { MONEY_RULE } from '../constants/formRule';
import { PayrollUtil } from '../../utils/PayrollUtil';

const { Paragraph, Text } = Typography;

const BudgetSubSection: React.FC = () => {
    const appCtx = useContext(AppContext);
    const kirinCtx = useContext(KirinContext);

    const api = new BudgetsApi(appCtx.configuration);
    const [budgetForm] = Form.useForm();

    const [total, setTotal] = useState<number>(0);

    useEffect(() => {
        if(kirinCtx.budgets) {
            setTotal(PayrollUtil.sumBudgets(kirinCtx.budgets));
            budgetForm.setFieldsValue({budgets: kirinCtx.budgets});
        }
    }, [kirinCtx.budgets, budgetForm]);

    const onFinish: FormProps["onFinish"] = (values: { budgets: Budget[] }) => {
        if(appCtx.debug) console.log('Success:', values);

        // Find budgets that exist in kirinCtx.budgets but not in values.budgets
        const budgetsToDelete = kirinCtx.budgets.filter((budget) => {
            return !values.budgets.some((valueBudget) => valueBudget.id === budget.id);
        });

        // Delete budgets
        const deletePromises = budgetsToDelete.map((budget) => {
            if (budget.id) {
                if (appCtx.debug) console.log("Deleting budget", budget);
                return api.deleteV1BudgetsId({ id: budget.id })
                    .then(() => {
                        if (appCtx.debug) console.log("Budget deleted", budget.id);
                    })
                    .catch((error) => {
                        if (appCtx.debug) console.error("Error deleting budget", error);
                        notification.error({ message: "Error deleting budget", description: error.message });
                    });
            }
        });
        // Update budgets
        const updatePromises = values.budgets.map((budget) => {
            if (values.budgets.length > 0) {
                if (!budget.id) {
                    if (appCtx.debug) console.log("Creating budget", budget);
                    budget.year = kirinCtx.currentYear;
                    return api.postV1Budgets({ budget: budget })
                        .then(() => {
                            if (appCtx.debug) console.log("Budget created");
                        })
                        .catch((error) => {
                            if (appCtx.debug) console.error("Error creating budget", error);
                            notification.error({ message: "Error creating budget", description: error.message });
                        });
                } else {
                    if (appCtx.debug) console.log("Updating budget", budget);
                    return api.putV1BudgetsId({ id: budget.id, budget: budget })
                        .then(() => {
                            if (appCtx.debug) console.log("Budget updated", budget.id);
                        })
                        .catch((error) => {
                            if (appCtx.debug) console.error("Error updating budget", error);
                            notification.error({ message: "Error updating budget", description: error.message });
                        });
                }
            }
        });

        Promise.all([...deletePromises, ...updatePromises])
        .then(() => {
            if (appCtx.debug) console.log("All budgets updated");
            notification.success({ message: "Budgets updated" });
            kirinCtx.setRefreshBudgets(!kirinCtx.refreshBudgets);
        })
        .catch((error) => {
            if (appCtx.debug) console.error("Error updating budgets", error);
            notification.error({ message: "Error updating budgets", description: error.message });
        });
    };

    return (
        <>
            <Divider className="header" orientation="center">Budgets</Divider>
            <Paragraph>
                Income can be used in fixed expenses and budgets. In this section we are going to define the budgets.
            </Paragraph>
             <Paragraph>
                Fixed expenses are those you have throughout the year and are predictable: taxes, fixed-cost services, 
                you can even calculate a high average of water and electricity consumption. Remember: it's better to 
                put more than expected.
            </Paragraph>
             <Paragraph>
                 The problem is the expenses that you can't predict, but you know you'll have: food, hobbies, 
                 gas/transportation, ... These are organized into budgets.               
            </Paragraph>
             <Paragraph>
                Budgets can be defined according to:
            </Paragraph>
            <ul>
                <li>
                    <b>Monthly</b>: You assign purchases each month, and what's available resets each month. This also 
                    means that these budgets use your total*12 months to be calculated. Money doesn't magically appear, 
                    sorry! These are useful for the expenses of the house (the frige doesn't fill itself!) or to define how much money you can spend 
                    on whatever you want, which is what you work for, right?
                </li>
                <li>
                    <b>Single</b>: You have a limited amount and when it's gone, it's gone! Useful for a 
                    vacation or for something expensive.
                </li>
            </ul>
            <Form
                form={budgetForm}
                labelCol={{ span: 8 }}
                wrapperCol={{ span: 16 }}
                onFinish={onFinish}
                autoComplete="off"
            >
                <Form.List name="budgets">
                    {(fields, { add, remove }) => (
                        <>
                            {fields.map(({ key, name, ...restField }) => (
                                <Space key={key} style={{ display: 'flex', marginBottom: 8 }} align="baseline">
                                    <Form.Item
                                        {...restField}
                                        name={[name, 'name']}
                                        rules={[{ required: true, message: 'Name is missing' }]}
                                    >
                                        <Input placeholder="Name" maxLength={50} />
                                    </Form.Item>
                                    <Form.Item
                                        {...restField}
                                        name={[name, 'budget']}
                                        rules={MONEY_RULE}
                                    >
                                        <InputNumber placeholder="Amount" addonAfter="€" />
                                    </Form.Item>
                                    <Form.Item
                                        {...restField}
                                        name={[name, 'type']}
                                        rules={[{ required: true, message: 'Missing type' }]}
                                    >
                                        <Select
                                            style={{ width: 120 }}
                                            options={[
                                                { value: 'MONTHLY', label: 'Monthly' },
                                                { value: 'SINGLE', label: 'Single' },
                                            ]}
                                        />
                                    </Form.Item>
                                    <MinusCircleOutlined onClick={() => remove(name)} />
                                </Space>
                            ))}
                            <Form.Item>
                                <Button type="dashed" onClick={() => add({type: 'MONTHLY',})} block icon={<PlusOutlined />}>
                                    Add budget
                                </Button>
                            </Form.Item>
                        </>
                    )}
                </Form.List>
                <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                    <Button type="primary" htmlType="submit">
                        Update budgets
                    </Button>
                </Form.Item>
            </Form>
            <Paragraph>
                <Text strong style={{fontSize: 16,}}>
                    Total budgets:
                </Text>
                <ul>
                    {total === 0 ? 
                        <li>You don't have any budget.</li> 
                        : <li>You've assigned a total of {total.toLocaleString()} €.</li>
                    }
                </ul>
            </Paragraph>
        </>
    );
};

export default BudgetSubSection;