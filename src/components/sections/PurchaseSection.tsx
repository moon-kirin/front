import React, { useContext, useEffect, useState } from 'react';
import { Collapse, Divider, Typography } from 'antd';
import { KirinContext } from '../../models/KirinContext';
import PurchaseSubSection from './PurchaseSubSection';

const { Paragraph } = Typography;

interface ItemType {
    key: number;
    label: string;
    children: JSX.Element;
}

const PurchaseSection: React.FC = () => {
    const kirinCtx = useContext(KirinContext);
    const [budgetPanels, setBudgetPanels] = useState<ItemType[]>([]);

    useEffect(() => {
        setBudgetPanels(kirinCtx.budgets.map((budget) => ({
            key: budget.id,
            label: budget.name,
            children: budget.type === 'MONTHLY' 
                ? <PurchaseSubSection budget={budget} monthly={true}  /> 
                : <PurchaseSubSection budget={budget} monthly={false} />
        })));
    }, [kirinCtx.budgets]);

    return (
        <>
            <Divider className="header" orientation="center">Purchases</Divider>
            <Paragraph>
                Without a doubt, this is the section that requires the most work, and it's the most important.
            </Paragraph>
            <Paragraph>
                Each purchase or expense must be assigned to a Budget, always remembering not to exceed their limit. 
                Be careful, you can, but don't blame Kirin if you don't get the numbers.
            </Paragraph>
            <Paragraph>
                Remember that there are two types of budgets:
            </Paragraph>
            <ul>
                <li><b>Monthly</b>: Amount is recharged each month, you must assign the expenses to the corresponding month.</li>
                <li><b>Unitary</b>: The amount is total, no matter in which month it was spent.</li>
            </ul>
            <Collapse items={budgetPanels} defaultActiveKey={['1']} />
        </>
    );
};

export default PurchaseSection;