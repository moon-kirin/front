import React, { useContext, useEffect, useState } from 'react';
import { Button, Divider, Form, FormProps, Input, InputNumber, Space, Typography, notification } from 'antd';
import { Saving, SavingApi } from '../../kirin';
import { AppContext } from '../../models/AppContext';
import { KirinContext } from '../../models/KirinContext';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { MONEY_RULE } from '../constants/formRule';
import { PayrollUtil } from '../../utils/PayrollUtil';

const { Paragraph, Text } = Typography;

const SavingsSubSection: React.FC = () => {
    const appCtx = useContext(AppContext);
    const kirinCtx = useContext(KirinContext);

    const api = new SavingApi(appCtx.configuration);
    const [savingForm] = Form.useForm();

    const [total, setTotal] = useState<number>(0);

    useEffect(() => {
        if(kirinCtx.savings) {
            setTotal(PayrollUtil.sumSavings(kirinCtx.savings));
            savingForm.setFieldsValue({savings: kirinCtx.savings});
        }
    }, [kirinCtx.savings, savingForm]);

    const onFinish: FormProps["onFinish"] = (values: { savings: Saving[] }) => {
        if(appCtx.debug) console.log('Success:', values);

        // Find savings that exist in kirinCtx.savings but not in values.savings
        const savingsToDelete = kirinCtx.savings.filter((saving) => {
            return !values.savings.some((valuesaving) => valuesaving.id === saving.id);
        });

        // Delete savings
        const deletePromises = savingsToDelete.map((saving) => {
            if (saving.id) {
                if (appCtx.debug) console.log("Deleting saving", saving);
                return api.deleteV1SavingsId({ id: saving.id })
                    .then(() => {
                        if (appCtx.debug) console.log("saving deleted", saving.id);
                    })
                    .catch((error) => {
                        if (appCtx.debug) console.error("Error deleting saving", error);
                        notification.error({ message: "Error deleting saving", description: error.message });
                    });
            }
        });
        // Update savings
        const updatePromises = values.savings.map((saving) => {
            if (values.savings.length > 0) {
                if (!saving.id) {
                    if (appCtx.debug) console.log("Creating saving", saving);
                    saving.year = kirinCtx.currentYear;
                    return api.postV1Savings({ saving: saving })
                        .then(() => {
                            if (appCtx.debug) console.log("saving created");
                        })
                        .catch((error) => {
                            if (appCtx.debug) console.error("Error creating saving", error);
                            notification.error({ message: "Error creating saving", description: error.message });
                        });
                } else {
                    if (appCtx.debug) console.log("Updating saving", saving);
                    return api.putV1SavingsId({ id: saving.id, saving: saving })
                        .then(() => {
                            if (appCtx.debug) console.log("Saving updated", saving.id);
                        })
                        .catch((error) => {
                            if (appCtx.debug) console.error("Error updating saving", error);
                            notification.error({ message: "Error updating saving", description: error.message });
                        });
                }
            }
        });

        Promise.all([...deletePromises, ...updatePromises])
        .then(() => {
            if (appCtx.debug) console.log("All savings updated");
            notification.success({ message: "Savings updated" });
            kirinCtx.setRefreshSavings(true);
        })
        .catch((error) => {
            if (appCtx.debug) console.error("Error updating savings", error);
            notification.error({ message: "Error updating savings", description: error.message });
        });
    };

    return (
        <>
            <Divider className="header" orientation="center">Savings</Divider>
            <Paragraph>
                Ok, we have defined the budgets that we will have during the year, but there are "unexpected" ones, so 
                we will have to move part of the income to save. The unexpected is beautiful, but it leads to ruin.
            </Paragraph>
             <Paragraph>
                Like budgets, define a name and annual total to save, this will be reduced from what is available 
                for the year. You can't assign purchases, so you cannot spend it, well you can, but don't do it!
            </Paragraph>
             <Paragraph>
                 If at the end of the year you have accumulated an extra according to what is indicated in this section 
                (or more), it means that you have done it well. Good job!
            </Paragraph>
             <Paragraph>
                As a recommendation, you should move at least 10% of your income to saving.
            </Paragraph>
            <Form
                form={savingForm}
                labelCol={{ span: 8 }}
                wrapperCol={{ span: 16 }}
                onFinish={onFinish}
                autoComplete="off"
            >
                <Form.List name="savings">
                    {(fields, { add, remove }) => (
                        <>
                            {fields.map(({ key, name, ...restField }) => (
                                <Space key={key} style={{ display: 'flex', marginBottom: 8 }} align="baseline">
                                    <Form.Item
                                        {...restField}
                                        name={[name, 'name']}
                                        rules={[{ required: true, message: 'Name is missing' }]}
                                    >
                                        <Input placeholder="Name" maxLength={50} />
                                    </Form.Item>
                                    <Form.Item
                                        {...restField}
                                        name={[name, 'amount']}
                                        rules={MONEY_RULE}
                                    >
                                        <InputNumber placeholder="Amount" addonAfter="€" />
                                    </Form.Item>
                                    <MinusCircleOutlined onClick={() => remove(name)} />
                                </Space>
                            ))}
                            <Form.Item>
                                <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                                    Add saving
                                </Button>
                            </Form.Item>
                        </>
                    )}
                </Form.List>
                <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                    <Button type="primary" htmlType="submit">
                        Update savings
                    </Button>
                </Form.Item>
            </Form>
            <Paragraph>
                <Text strong style={{fontSize: 16,}}>
                    Total savings:
                </Text>
                <ul>
                    {total === 0 ? 
                        <li>Are you sure you don't want to save anything? Your future broken fridge is ok with that.</li> 
                        : <li>You're going to save up to {total.toLocaleString()} € this year.</li>
                    }
                </ul>
            </Paragraph>
        </>
    );
};

export default SavingsSubSection;