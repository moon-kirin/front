import React from 'react';
import FixedExpensesSubSection from './FixedExpensesSubSection';
import FixedExpensesSummarySubSection from './FixedExpensesSummarySubSection';
const FixedExpensesSection: React.FC = () => {
    return (
        <>
            <FixedExpensesSubSection />
            <FixedExpensesSummarySubSection />
        </>
    );
};

export default FixedExpensesSection;