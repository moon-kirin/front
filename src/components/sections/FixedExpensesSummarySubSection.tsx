import React from 'react';
import { Divider, Typography } from 'antd';
import FixedExpensesChart from '../helpers/FixedExpensesChart';

const { Paragraph } = Typography;

const FixedExpensesSummarySubSection: React.FC = () => {
  return (
      <>
        <Divider className="header" orientation="center">Summary</Divider>  
        <Paragraph>
            If we consider the incomes along with the budgets and savings you had allocated, 
            and add to that the fixed expenses you have allocated, this is the result:
        </Paragraph>
        <FixedExpensesChart />
      </>
  );
};

export default FixedExpensesSummarySubSection;