import React, { useContext } from 'react';
import { Col, Result, Row, Typography } from 'antd';
import { AccountBookOutlined, ShopOutlined, WalletOutlined } from '@ant-design/icons';
import { AppContext } from '../../models/AppContext';
import MarkedExpenses from '../helpers/MarkedExpenses';

const { Paragraph, Text } = Typography;

const HomeSection: React.FC = () => {
    const appCtx = useContext(AppContext);
    const subtitle = appCtx.status === 'empty' ? "This is your first time here, so first you have to define your first year." : 
        "Ready to add more information or revise your plans?";
    return (
        <Row>
            <Col span={appCtx.status !== 'empty' ? 16 : 24}>
                <Result
                    icon={null}
                    title="Welcome to Kirin!"
                    subTitle={subtitle}
                >
                    <div>
                        <Paragraph>
                            If you're here, it's either by mistake or because you're tired of not understanding where your money has gone. 
                            I can't help you with the former, but I certainly can help you with the latter.
                        </Paragraph>
                        <Paragraph>
                            So, what does this service do?
                        </Paragraph>
                        <Paragraph>
                            Kirin is basically a database where you store the information of everything you have bought during the year, 
                            and thus survive the end of the month, and the end of the year, without going bankrupt!
                        </Paragraph>
                        <Paragraph>
                            And how does he do it? Well, unfortunately the alchemical formula of turning stones into gold is not available, but 
                            it helps you understand where your money went (and you don't need alchemy to know that money does fly).
                        </Paragraph>
                        <Paragraph>
                            Of course, understanding where your money went isn't enough, you must sort and plan it. If you don't like planning, 
                            run away! You have better things to do than waste your time here.
                        </Paragraph>
                        <Paragraph>
                            Ok, so it's good for planning your money, how do you do it? Well, in three steps:
                        </Paragraph>
                        <ul>
                            <li>
                                <AccountBookOutlined /> <b>Payroll</b> is where you add all your income (payroll during the year) and any extras. With 
                                this you already know how much money you will make during the year (I hope it is a lot!), then you must divide it 
                                between budgets that will allow you to spend it during the year and on what you want to save.
                            </li>
                            <li>
                                <ShopOutlined /> During the year there are <b>expenses</b> that are <b>fixed</b>, you don't need to wait to find out when 
                                and how much taxes cost you, the services you have contracted or even the average cost of electricity. So, you can define 
                                them from the beginning and one less thing. Remember that <span className="error-icon">your income – budgets – savings – 
                                fixed expenses must not be negative!</span>
                            </li>
                            <li>
                                <WalletOutlined /> Remember the <b>budgets</b> we created at the beginning? Well, these are spent in the Budgets section, every 
                                time you buy something you must enter it in their Budget, so they will inform you how much money they have left.
                            </li>
                        </ul>
                        <Paragraph>
                            And that's it! With that amount of data, you will be able to understand where your money went and how much you have left from 
                            here and there.
                        </Paragraph>
                        <Paragraph>
                            Oh, and a word of advice: being meticulous is the only way Kirin will work, if you don't add everything you buy, the data won't be real.
                        </Paragraph>
                        {appCtx.status === 'empty' && (
                            <Paragraph>
                                <Text strong style={{fontSize: 16,}}>
                                    If you're ready for this, start by creating a new year from the side menu.
                                </Text>
                            </Paragraph>
                        )}
                    </div>
                </Result>
            </Col>
            {appCtx.status !== 'empty' && (
                <Col span={8}>
                    <MarkedExpenses />
                </Col>
            )}
        </Row>
        
    );
};

export default HomeSection;