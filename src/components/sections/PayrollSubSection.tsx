import React, { useContext, useEffect, useState } from 'react';
import { Button, Col, Divider, Form, FormProps, Input, Row, Space, Typography, notification } from 'antd';
import { PayrollApi } from '../../kirin';
import { AppContext } from '../../models/AppContext';
import { KirinContext } from '../../models/KirinContext';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { PayrollUtil } from '../../utils/PayrollUtil';
import { MONEY_RULE } from '../constants/formRule';

const { Paragraph, Text } = Typography;

const PayrollSubSection: React.FC = () => {
    const appCtx = useContext(AppContext);
    const kirinCtx = useContext(KirinContext);

    const api = new PayrollApi(appCtx.configuration);
    const [payrollForm] = Form.useForm();

    const [totalMonths, setTotalMonths] = useState<number>(0);
    const [totalExtra, setTotalExtra] = useState<number>(0);

    useEffect(() => {
        if(kirinCtx.payroll) {
            setTotalMonths(PayrollUtil.sumMonths(kirinCtx.payroll));
            setTotalExtra(PayrollUtil.sumExtra(kirinCtx.payroll));
            payrollForm.setFieldsValue(kirinCtx.payroll);
        }
    }, [kirinCtx.payroll, payrollForm]);

    const onFinish: FormProps["onFinish"] = (values) => {
        if(appCtx.debug) console.log('Success:', values);
        api.putV1PayrollsYear({year: kirinCtx.currentYear, payroll: values}).then(() => {
            if(appCtx.debug) console.log("Payroll updated");
            kirinCtx.setPayroll(values);
            notification.success({
                message: 'Payroll updated',
            });
        })
        .catch(error => {
            console.error("error", error);
            notification.error({
                message: 'Error updating payroll',
                description: 'There was an error updating the payroll. Check the logs.',
            });
        });
    };

    function handleCopyMonths(): void {
        const januaryValue = payrollForm.getFieldValue('payroll01');
        const updatedValues = {
            payroll01: januaryValue,
            payroll02: januaryValue,
            payroll03: januaryValue,
            payroll04: januaryValue,
            payroll05: januaryValue,
            payroll06: januaryValue,
            payroll07: januaryValue,
            payroll08: januaryValue,
            payroll09: januaryValue,
            payroll10: januaryValue,
            payroll11: januaryValue,
            payroll12: januaryValue,
        };
        payrollForm.setFieldsValue(updatedValues);
    }

    return (
        <>
            <Divider className="header" orientation="center">Payroll</Divider>
            <Paragraph>
                Payroll is the starting point and the most important: where all income is defined. Because without income, 
                we won't spend much.
            </Paragraph>
            <Paragraph>
                The idea is to plan all year, here are some recommendations:
            </Paragraph>
            <ul>
                <li>At the beginning of the year, define the income expected by that year.</li>
                <li>If any of them may vary, fill the expected minimum to avoid surprises. The values can be changed at any time, 
                    but it is better to make the mistake of defining less, than to put too much and not have enough.</li>
                <li>Fill the income for each month.</li>
                <li>Extra income that is not associated with a specific month is added in its section.</li>
            </ul>
            <Form
                form={payrollForm}
                labelCol={{ span: 8 }}
                wrapperCol={{ span: 16 }}
                onFinish={onFinish}
                autoComplete="off"
            >
                <Divider orientation="left">Months</Divider>
                <p>
                    Indicate the income for each month.
                </p>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label="January" name="payroll01" rules={MONEY_RULE}>
                            <Input type='number' addonAfter="€" />
                        </Form.Item>
                        <Form.Item label="February" name="payroll02" rules={MONEY_RULE}>
                            <Input type='number' addonAfter="€" />
                        </Form.Item>
                        <Form.Item label="March" name="payroll03" rules={MONEY_RULE}>
                            <Input type='number' addonAfter="€" />
                        </Form.Item>
                        <Form.Item label="April" name="payroll04" rules={MONEY_RULE}>
                            <Input type='number' addonAfter="€" />
                        </Form.Item>
                        <Form.Item label="May" name="payroll05" rules={MONEY_RULE}>
                            <Input type='number' addonAfter="€" />
                        </Form.Item>
                        <Form.Item label="June" name="payroll06" rules={MONEY_RULE}>
                            <Input type='number' addonAfter="€" />
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                         <Form.Item label="July" name="payroll07" rules={MONEY_RULE}>
                            <Input type='number' addonAfter="€" />
                        </Form.Item>
                        <Form.Item label="August" name="payroll08" rules={MONEY_RULE}>
                            <Input type='number' addonAfter="€" />
                        </Form.Item>
                        <Form.Item label="September" name="payroll09" rules={MONEY_RULE}>
                            <Input type='number' addonAfter="€" />
                        </Form.Item>
                        <Form.Item label="October" name="payroll10" rules={MONEY_RULE}>
                            <Input type='number' addonAfter="€" />
                        </Form.Item>
                        <Form.Item label="November" name="payroll11" rules={MONEY_RULE}>
                            <Input type='number' addonAfter="€" />
                        </Form.Item>
                        <Form.Item label="December" name="payroll12" rules={MONEY_RULE}>
                            <Input type='number' addonAfter="€" />
                        </Form.Item>
                    </Col>
                </Row>
                <Form.Item>
                    <Button type="default" onClick={handleCopyMonths}>
                        Copy the January value in all months
                    </Button>
                </Form.Item>
                
                <Divider orientation="left">Extra income</Divider>
                <p>
                    If in addition to the fixed months you have extra income (like Christmas bonus), define its name and income.
                </p>
                <Form.List name="extra">
                    {(fields, { add, remove }) => (
                        <>
                            {fields.map(({ key, name, ...restField }) => (
                                <Space key={key} style={{ display: 'flex', marginBottom: 8 }} align="baseline">
                                    <Form.Item
                                        {...restField}
                                        name={[name, 'name']}
                                        rules={[{ required: true, message: 'Name is missing' }]}
                                    >
                                        <Input placeholder="Name" maxLength={50} />
                                    </Form.Item>
                                    <Form.Item
                                        {...restField}
                                        name={[name, 'payroll']}
                                        rules={MONEY_RULE}
                                    >
                                        <Input placeholder="Income" type='number' addonAfter="€" />
                                    </Form.Item>
                                    <MinusCircleOutlined onClick={() => remove(name)} />
                                </Space>
                            ))}
                            <Form.Item>
                                <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                                Add extra income
                                </Button>
                            </Form.Item>
                        </>
                    )}
                </Form.List>
                <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                <Button type="primary" htmlType="submit">
                    Update payroll
                </Button>
                </Form.Item>
                <Paragraph>
                    <Text strong style={{fontSize: 16,}}>
                        Total revenue:
                    </Text>
                    <ul>
                        <li>Over the months you get {totalMonths.toLocaleString()} €</li>
                        {totalExtra === 0 ? 
                            <li>You don't have any extra income.</li> 
                            : <li>Added to this is {totalExtra.toLocaleString()} € extra income.</li>
                        }
                        {totalMonths+totalExtra === 0 ? 
                            <li>Your income sum is 0, so either you haven't entered the income yet, or you haven't found a job yet</li>
                            : <li>Which adds up to a total of {(totalMonths+totalExtra).toLocaleString()} €</li>
                        }
                    </ul>
                </Paragraph>
            </Form>
        </>
    );
};

export default PayrollSubSection;