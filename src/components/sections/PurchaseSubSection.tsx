import React, { useContext, useEffect, useState } from 'react';
import { Button, Modal, Form, Input, InputNumber, Select, notification } from 'antd';
import { MinusCircleOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { Budget, Purchase, PurchaseApi } from '../../kirin';
import { KirinContext } from '../../models/KirinContext';
import { AppContext } from '../../models/AppContext';
import PurchaseMonth from '../helpers/PurchaseMonth';
import PurchaseSummary from '../helpers/PurchaseSummary';
import { CategoryUtil } from '../../utils/CategoryUtil';
import { monthNames } from '../constants/constants';
import FormCommons from '../helpers/FormCommons';
import { ColumnsType } from 'antd/es/table';

const { render } = CategoryUtil();
const { Option } = Select;

interface PurchaseSubSection {
    budget: Budget;
    monthly: boolean;
}

const PurchaseSubSection: React.FC<PurchaseSubSection> = ({ budget, monthly }) => {
    const appCtx = useContext(AppContext);
    const kirinCtx = useContext(KirinContext);

    const api = new PurchaseApi(appCtx.configuration);
    const [form] = Form.useForm();

    const [purchases, setPurchases] = useState<Purchase[]>([]);
    const [refresh, setRefresh] = useState(true);
    const [visible, setVisible] = useState(false);

    useEffect(() => {
        if(refresh) {
            setRefresh(false);
            api.getV1Purchases({ budgetId: budget.id }).then((response: Purchase[]) => {
                if(appCtx.debug) console.log("Purchases", response);
                setPurchases(response);
            });
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [refresh]);

    const columns: ColumnsType<Purchase> = [
        { 
            title: 'Name', 
            dataIndex: 'name', 
            key: 'name',
            sorter: (a: Purchase, b: Purchase) => a.name.localeCompare(b.name),
         },
        { title: 'Description', dataIndex: 'description', key: 'description' },
        { 
            title: 'Category', 
            dataIndex: 'categoryId', 
            key: 'categoryId',
            render: (categoryId: number) => render(categoryId, kirinCtx.categories),
            sorter: (a: Purchase, b: Purchase) => a.categoryId - b.categoryId,
            defaultSortOrder: 'ascend',
        },
        { 
            title: 'Price', 
            dataIndex: 'price', 
            key: 'price', 
            sorter: (a: Purchase, b: Purchase) => a.price - b.price,
        },
        {
            title: 'Action',
            key: 'action',
            render: (text: string, record: Purchase) => (
                <>
                    <Button onClick={() => handleEdit(record)} icon={<EditOutlined />} />
                    <Button onClick={() => handleDelete(record)} icon={<MinusCircleOutlined />} />
                </>
            ),
        },
    ];

    const [modalTitle, setModalTitle] = useState("");
    const [modalActionAdd, setModalActionAdd] = useState(true);

    const handleAdd = () => {
        form.resetFields();
        setModalTitle("Create new Purchase");
        setModalActionAdd(true);
        setVisible(true);
        
    };

    const handleEdit = (record: Purchase) => {
        setModalTitle(`Edit the Purchase: ${record.name}`);
        setModalActionAdd(false);
        setVisible(true);

        form.setFieldsValue(record);
    };

    const handleDelete = (record: Purchase) => {
        // delete record
        api.deleteV1PurchasesId({id: record.id}).then(() => {
            if(appCtx.debug) console.log("Purchase deleted");
            setPurchases(purchases.filter(expense => expense.id !== record.id));

            notification.success({message: 'Purchase deleted',});
        })
        .catch(error => {
            console.error("error", error);
            notification.error({
                message: 'Error deleting the purchase',
                description: 'There was an error deleting the purchase. Check the logs.',
            });
        });
    };

    const handleOk = () => {
        form.validateFields().then(values => {
            if(appCtx.debug) console.log('Success:', values);
            values.year = kirinCtx.currentYear;
            values.budgetId = budget.id;

            modalActionAdd ? handleApiAdd(values) : handleApiEdit(values);
        }).catch(info => {
            console.log('Validate Failed:', info);
        });
    };

    const handleApiAdd = (values: Purchase) => {  
        api.postV1Purchases({purchase: values}).then(response => {
            if(appCtx.debug) console.log("Purchase created");

            setRefresh(true);

            notification.success({message: 'Purchase created',});
            setVisible(false);
        })
        .catch(error => {
            console.error("error", error);
            notification.error({
                message: 'Error creating the purchase',
                description: 'There was an error creating the purchase. Check the logs.',
            });
        });
    };
    const handleApiEdit = (values: Purchase) => {
        console.log(values);
        api.putV1PurchasesId({id: values.id, purchase: values}).then(() => {
            if(appCtx.debug) console.log("Purchase updated");
            const updatedPurchases = purchases.map(expense => {
                if (expense.id === values.id) {
                    return values;
                }
                return expense;
            });
            setPurchases(updatedPurchases);

            notification.success({message: 'Purchase updated',});
            setVisible(false);
        })
        .catch(error => {
            console.error("error", error);
            notification.error({
                message: 'Error updating the purchase',
                description: 'There was an error updating the purchase. Check the logs.',
            });
        });
    };

    return (
        <div>
            <Button type="primary" onClick={handleAdd} block icon={<PlusOutlined />}>
                Add
            </Button>
            <PurchaseMonth purchases={purchases} columns={columns} monthly={monthly} />
            
            <Modal title={modalTitle} open={visible} onOk={handleOk} onCancel={() => setVisible(false)}>
                <Form form={form} layout="horizontal" labelCol={{ span: 6 }} initialValues={{ month: monthly ? new Date().getMonth() + 1 : 0 }}>
                    <Form.Item name="month" label="Month" style={{ display: monthly ? 'block' : 'none' }} rules={[{ required: true }]}>
                        {monthly ? (
                            <Select>
                                {monthNames.map((name, index) => (
                                    <Option value={index + 1} key={name}>
                                        {name}
                                    </Option>
                                ))}
                            </Select>
                        ) : (
                            <Input type="hidden"  />
                        )}
                    </Form.Item>
                    <FormCommons />
                    <Form.Item name="price" label="Price" rules={[{ required: true }]}>
                        <InputNumber />
                    </Form.Item>
                </Form>
            </Modal>

            <PurchaseSummary purchases={purchases} monthly={monthly} budget={budget} />
        </div>
    );
};

export default PurchaseSubSection;