import React, { useContext, useEffect, useState } from 'react';
import { Divider, Typography } from 'antd';
import { FixedExpenseTypeEnum, Purchase, PurchaseApi } from '../../kirin';
import { AppContext } from '../../models/AppContext';
import { KirinContext } from '../../models/KirinContext';
import FixedExpensesChart from '../helpers/FixedExpensesChart';
import PurchasesCharts from '../helpers/PurchasesCharts';
import BasicPie from '../helpers/BasicPie';

const { Paragraph } = Typography;

const DashboardSection: React.FC = () => {
    const appCtx = useContext(AppContext);
    const kirinCtx = useContext(KirinContext);

    const api = new PurchaseApi(appCtx.configuration);

    const [budgetIds, setBudgetIds] = useState<number[]>([]);
    const [purchasesByBudget, setPurchasesByBudget] = useState({});

    const [purchasesPieData, setPurchasesPieData] = useState([]);


    // Extract the budget ids
    useEffect(() => {
        setBudgetIds(kirinCtx.budgets.map(budget => budget.id));
    }, [kirinCtx.budgets]);

    // Load the purchases of all budgets
    useEffect(() => {
        Promise.all(budgetIds.map(budgetId => 
            api.getV1Purchases({ budgetId }).then((response: Purchase[]) => ({ budgetId, response }))
        )).then(processPurchases);
    }, [budgetIds]);

    const processPurchases = (results: { budgetId: number, response: Purchase[] }[]) => {
        const newPurchasesByBudget = results.reduce((acc, { budgetId, response }) => {
            acc[budgetId] = response;
            return acc;
        }, {} as { [key: number]: Purchase[] });
        setPurchasesByBudget(newPurchasesByBudget);

        const newCategoryTotals = Object.values(newPurchasesByBudget).flat().reduce((acc, purchase) => {
            if (!acc[purchase.categoryId]) {
                acc[purchase.categoryId] = 0;
            }
            acc[purchase.categoryId] += purchase.price;
            return acc;
        }, {} as { [key: number]: number });

        const newPurchasesPieData = Object.entries(newCategoryTotals).map(([categoryId, total]) => {
            const category = kirinCtx.categories.find(category => category.id === Number(categoryId));
            return {
                id: category.name,
                value: Number(total) || 0, // Ensure the value is numeric
            };
        });

        setPurchasesPieData(newPurchasesPieData);
    }
    const [fixedExpensesPieData, setFixedExpensesPieData] = useState([]);

    useEffect(() => {
        const newFixedExpensesPieData = Object.values(kirinCtx.fixedExpenses).reduce((acc, expense) => {
            const category = kirinCtx.categories.find(category => category.id === expense.categoryId);
            const categoryName = category ? category.name : 'Unknown';
            let total = expense.price;

            if (expense.type === FixedExpenseTypeEnum.Monthly) {
                total *= expense.months.length;
            }

            const existingCategory = acc.find(item => item.id === categoryName);
            if (existingCategory) {
                existingCategory.value += total;
            } else {
                acc.push({
                    id: categoryName,
                    value: total,
                });
            }

            return acc;
        }, []);
        setFixedExpensesPieData(newFixedExpensesPieData);
    }, [kirinCtx.fixedExpenses, kirinCtx.categories]);

    const [combinedData, setCombinedData] = useState([]);

    useEffect(() => {
        const newCombinedData = [...purchasesPieData, ...fixedExpensesPieData].reduce((acc, item) => {
            const existingCategory = acc.find(category => category.id === item.id);
            if (existingCategory) {
                existingCategory.value += item.value;
            } else {
                acc.push({...item});
            }

            return acc;
        }, []);

        setCombinedData(newCombinedData);
    }, [purchasesPieData, fixedExpensesPieData]);

    return (
        <>
            <Paragraph>
                Because it's not easy to know where your money has gone, from this section you can quickly see where you have your 
                money allocated, if your budgets are in expected figures, and which categories are the ones that blow your wallet.
            </Paragraph>
            <Paragraph>
                First, let's look at the totals according to the allocations. Here it is advisable to leave a certain margin 
                unallocated, so that a calculation error or data not entered can be mitigated by it.
            </Paragraph>
            <Paragraph>
                The year is very long, so forgetting something or having unexpected things is more normal than we would like to say.
            </Paragraph>
            <FixedExpensesChart />
            {budgetIds.length > 0 && Object.keys(purchasesByBudget).length > 0 && (
                <>
                    <Divider className="header" orientation="center">Purchases</Divider>
                    <Paragraph>
                        Budgets can't exceed their limit. If they do exceed it, can we accept that they spent it on someone else's?
                    </Paragraph>
                    <PurchasesCharts purchases={purchasesByBudget} budgets={kirinCtx.budgets}/>
                    
                </>
            )}
            <div>
                <Divider className="header" orientation="center">Categories</Divider>
                <Paragraph>
                    Knowing which categories are the ones that attract your money the most is important to know how to counteract 
                    them, or simply accept that the money is well invested in it.
                </Paragraph>
                {combinedData.length > 0 && (
                    <BasicPie data={combinedData} />
                )}
                {fixedExpensesPieData.length > 0 && (
                    <>
                        <Divider orientation="left">For Fixed Expenses</Divider>
                        <Paragraph>
                            Total Spent on Fixed Expenses
                        </Paragraph>
                        <BasicPie data={fixedExpensesPieData} />
                    </>
                )}
                {purchasesPieData.length > 0 && (
                    <>
                        <Divider orientation="left">For Purchases</Divider>
                        <Paragraph>
                            Total Spent on Budgets
                        </Paragraph>
                        <BasicPie data={purchasesPieData} />
                    </>
                )}
            </div>

        </>
    );
};

export default DashboardSection;