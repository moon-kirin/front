import React, { useContext, useEffect, useState } from 'react';
import { Divider, Typography } from 'antd';
import { KirinContext } from '../../models/KirinContext';
import SummaryBasicChart from '../helpers/SummaryBasicChart';
import { PayrollUtil } from '../../utils/PayrollUtil';

const { Paragraph } = Typography;

const PayrollSummarySubSection: React.FC = () => {
    const kirinCtx = useContext(KirinContext);

    const [totalPayroll, setTotalPayroll] = useState<number>(0);
    const [totalBudgets, setTotalBudgets] = useState<number>(0);
    const [totalSavings, setTotalSavings] = useState<number>(0);

    useEffect(() => {
        if(kirinCtx.payroll) {
            const totalMonths = PayrollUtil.sumMonths(kirinCtx.payroll);
            const totalExtra = PayrollUtil.sumExtra(kirinCtx.payroll);
            setTotalPayroll(totalMonths+totalExtra);

            setTotalBudgets(PayrollUtil.sumBudgets(kirinCtx.budgets));
            setTotalSavings(PayrollUtil.sumSavings(kirinCtx.savings));
        }
    }, [kirinCtx.payroll, kirinCtx.budgets, kirinCtx.savings]);

    const data = [
        {
            "category": "Payroll",
            "Budget": totalBudgets,
            "Savings": totalSavings,
            "Unallocated": totalPayroll - totalBudgets - totalSavings,
        },
    ];

    return (
        <>
            <Divider className="header" orientation="center">Summary</Divider>
            <Paragraph>
                Here you have divided your income according to your budgets and savings. The rest is unallocated and 
                needed for fixed expenses, so remember that value shouldn't be 0!
            </Paragraph>
            <SummaryBasicChart keys={["Budget", "Savings", "Unallocated"]} data={data} total={totalPayroll} />
        </>
    );
};

export default PayrollSummarySubSection;