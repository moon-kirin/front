export const MONEY_PATTERN = /^\d+(?:\.\d{0,2})?$/;
export const MONEY_RULE = [{ required: true, message: 'The field is required and must match with the currency format', pattern: MONEY_PATTERN }]
