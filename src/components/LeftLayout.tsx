import React, { useContext, useEffect, useState } from 'react';
import { TTokenData } from 'react-oauth2-code-pkce/dist/types';
import { useLocation, useNavigate } from 'react-router-dom';
import { Menu, MenuProps } from 'antd';
import Sider from 'antd/es/layout/Sider';
import MenuItem from 'antd/es/menu/MenuItem';
import { AppContext } from '../models/AppContext';
import { BreadcrumbUtil } from '../utils/BreadcrumbUtil';
import AddYear from './helpers/AddYear';
import Logo from './helpers/Logo';
import UserLogo from './helpers/UserLogo';
import Logout from './helpers/Logout';
import YearSelector from './helpers/YearSelector';
import { HomeOutlined, WalletOutlined, ShopOutlined, AccountBookOutlined, PieChartOutlined } from '@ant-design/icons';

interface LeftLayoutProps {
    logout: () => void;
    tokenData: TTokenData;
}

type MenuItem = Required<MenuProps>['items'][number];

function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  disabled?: boolean,
): MenuItem {
  return {
    key,
    icon,
    label,
    disabled,
  } as MenuItem;
}

const items: MenuItem[] = [
  getItem('Home', '', <HomeOutlined />),
  getItem('Payroll', 'payroll', <AccountBookOutlined />),
  getItem('Fixed Expenses', 'fixed-expenses', <ShopOutlined />),
  getItem('Purchase', 'purchase', <WalletOutlined />),
  getItem('Dashboard', 'dashboard', <PieChartOutlined />)
];

const LeftLayout: React.FC<LeftLayoutProps> = ({ logout, tokenData }) => {
  const appCtx = useContext(AppContext);
  const [collapsed, setCollapsed] = useState(false);

  const urlPath = useLocation().pathname.substring(1);
  const navigate = useNavigate();

  const regenBreadcrumb = (id: string) => {
    const selectedMenuItem = items.find(item => item?.key === id);
    if(selectedMenuItem) {
      appCtx.setBreadcrumb(BreadcrumbUtil.get(selectedMenuItem.key.toString(), selectedMenuItem.key.toString()));
    }
  }
  //Update breadcrumb when page is loaded
  useEffect(() => {
    regenBreadcrumb(urlPath);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  //Update breadcrumb and URL when menu is selected
  const handleMenuSelect: MenuProps['onSelect'] = (e: Parameters<MenuProps['onSelect']>[0]) => {
    regenBreadcrumb(e.key);
    navigate(`/${e.key}`);
  };

  let content;
  if (appCtx.status === 'empty') {
    content = <AddYear />;
  } else if (appCtx.status === 'ok') {
    content = (
      <>
        <YearSelector />
        <AddYear />
        <Menu theme="dark" defaultSelectedKeys={[urlPath]} mode="inline" items={items} onSelect={handleMenuSelect} />
      </>
    );
  }

  return (
    <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
      <Logo />
      <UserLogo collapsed={collapsed} tokenData={tokenData} />
      <Logout logout={logout} />
      {content}
    </Sider>
  );
};

export default LeftLayout;