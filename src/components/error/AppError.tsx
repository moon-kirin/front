import React from 'react';
import { Result, Typography } from 'antd';
import { BugOutlined, CloseCircleOutlined, SmileOutlined } from '@ant-design/icons';

const { Paragraph, Text } = Typography;

const AppError: React.FC = () => {
    return (
        <Result
            status="500"
            title="Error loading data"
            subTitle="Kirin cannot load the data from the service. Perhaps the service configuration is incorrect or it is stopped."
        >
            <div>
                <Paragraph>
                    <Text strong style={{fontSize: 16,}}>
                        If you are NOT the Kirin administrator:
                    </Text>
                </Paragraph>
                <Paragraph>
                    <BugOutlined className="error-icon" /> It's not your problem! Find the administrator and complain about it.
                </Paragraph>
                <Paragraph>
                    <BugOutlined className="error-icon" /> Remember: Kirin is not responsible for any damage caused by the administrator to your person. <SmileOutlined />
                </Paragraph>
                <Paragraph>
                    <Text strong style={{fontSize: 16,}}>
                        If you ARE the Kirin administrator:
                    </Text>
                </Paragraph>
                <Paragraph>
                    <CloseCircleOutlined className="error-icon" /> Check the browser console, if the URL to the service is misconfigured or not exist, it will say so.
                </Paragraph>
                <Paragraph>
                    <CloseCircleOutlined className="error-icon" /> Check if the Kirin backend service is working, if so, check the logs.
                </Paragraph>
                <Paragraph>
                    <CloseCircleOutlined className="error-icon" /> If nothing works and no one has seen the error, pretend you haven't seen it!
                </Paragraph>
            </div>
        </Result>
    );
};

export default AppError;