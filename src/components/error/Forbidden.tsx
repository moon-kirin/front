import React from 'react';
import { Result, Typography } from 'antd';
import { BugOutlined, CloseCircleOutlined, FrownOutlined, SmileOutlined } from '@ant-design/icons';

const { Paragraph, Text } = Typography;

const Forbidden: React.FC = () => {
    return (
        <Result
            status="403"
            title="Forbidden access"
            subTitle="Sorry, your user does not have the necessary permissions to access."
        >
            <div>
                <Paragraph>
                    <Text strong style={{fontSize: 16,}}>
                        If you are NOT the Kirin administrator:
                    </Text>
                </Paragraph>
                <Paragraph>
                    <BugOutlined className="error-icon" /> It's not your problem! Find the administrator and complain about it.
                </Paragraph>
                <Paragraph>
                    <BugOutlined className="error-icon" /> Remember: Kirin is not responsible for any damage caused by the administrator to your person. <SmileOutlined />
                </Paragraph>
                <Paragraph>
                    <Text strong style={{fontSize: 16,}}>
                        If you ARE the Kirin administrator:
                    </Text>
                </Paragraph>
                <Paragraph>
                    <CloseCircleOutlined className="error-icon" /> Bad start if you don't give yourself permissions. <FrownOutlined />
                </Paragraph>
                <Paragraph>
                    <CloseCircleOutlined className="error-icon" /> Check if the Identity Provider add the necessary permissions to your user. If you are using Keycloak, is the <b>`kirin:user`</b> global role.
                </Paragraph>
            </div>
        </Result>
    );
};

export default Forbidden;