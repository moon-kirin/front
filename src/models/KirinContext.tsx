import { createContext } from 'react';
import { OptionProps } from 'antd/es/select';
import { Budget, Category, FixedExpense, Payroll, Saving } from '../kirin';

type KirinContextType = {
  currentYear: number;
  changeCurrentYear: (newCurrentYear: number) => void;
  listYears: OptionProps[];
  setListYears: React.Dispatch<React.SetStateAction<OptionProps[]>>;
  reloadListYears: boolean;
  setReloadListYears: React.Dispatch<React.SetStateAction<boolean>>;
  payroll: Payroll | undefined;
  setPayroll: React.Dispatch<React.SetStateAction<Payroll | undefined>>;
  budgets: Budget[];
  setBudgets: React.Dispatch<React.SetStateAction<Budget[]>>;
  refreshBudgets: boolean;
  setRefreshBudgets: React.Dispatch<React.SetStateAction<boolean>>;
  savings: Saving[];
  setSavings: React.Dispatch<React.SetStateAction<Saving[]>>;
  refreshSavings: boolean;
  setRefreshSavings: React.Dispatch<React.SetStateAction<boolean>>;
  fixedExpenses: FixedExpense[];
  setFixedExpenses: React.Dispatch<React.SetStateAction<FixedExpense[]>>;
  refreshFixedExpenses: boolean;
  setRefreshFixedExpenses: React.Dispatch<React.SetStateAction<boolean>>;
  categories: Category[]; 
  setCategories: React.Dispatch<React.SetStateAction<Category[]>>;
};

const defaultKirinContextValue: KirinContextType = {
  currentYear: 0,
  changeCurrentYear: () => {},
  listYears: [],
  setListYears: () => {},
  reloadListYears: false,
  setReloadListYears: () => {},
  payroll: undefined,
  setPayroll: () => {},
  budgets: [],
  setBudgets: () => {},
  refreshBudgets: false,
  setRefreshBudgets: () => {},
  savings: [],
  setSavings: () => {},
  refreshSavings: false,
  setRefreshSavings: () => {},
  fixedExpenses: [],
  setFixedExpenses: () => {},
  refreshFixedExpenses: false,
  setRefreshFixedExpenses: () => {},
  categories: [],
  setCategories: () => {},
};

export const KirinContext = createContext<KirinContextType>(defaultKirinContextValue);
