import { createContext } from 'react';
import { ItemType } from 'antd/es/breadcrumb/Breadcrumb';
import { Configuration } from '../kirin';

type AppContextType = {
  debug: boolean;
  configuration: Configuration;
  status: string;
  setStatus: React.Dispatch<React.SetStateAction<string>>;
  breadcrumb: ItemType[] | undefined;
  setBreadcrumb: React.Dispatch<React.SetStateAction<ItemType[] | undefined>>;
};

const defaulAppContextValue: AppContextType = {
  debug: false,
  configuration: new Configuration(),
  status: '',
  setStatus: () => {},
  breadcrumb: [],
  setBreadcrumb: () => {},
};

export const AppContext = createContext<AppContextType>(defaulAppContextValue);