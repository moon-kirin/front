/* tslint:disable */
/* eslint-disable */
/**
 * Kirin
 * This service allows you to define the monthly and extra income during the year to organize them in different fixed costs and budgets. For each budget you can add purchases made. A Payroll must be previously defined to be able to create new objects associated with that year, if not, they will return a conflict error.  This application does not activate alerts (for example when exceeding the limit of a budget), it offers the ability to organize the data and the user is free to exceed or adjust as they consider.   The data entered is assigned to the user identifier, so no other user can access.  Only production servers are published to the internet.
 *
 * The version of the OpenAPI document: 1.1
 * Contact: rei.izumi@moon.cat
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


import * as runtime from '../runtime';
import type {
  BadRequest,
  Payroll,
  Year,
} from '../models';
import {
    BadRequestFromJSON,
    BadRequestToJSON,
    PayrollFromJSON,
    PayrollToJSON,
    YearFromJSON,
    YearToJSON,
} from '../models';

export interface DeleteV1PayrollsYearRequest {
    year: number;
}

export interface GetV1PayrollsYearRequest {
    year: number;
}

export interface PostV1PayrollsYearRequest {
    year: number;
    payroll?: Payroll;
}

export interface PutV1PayrollsYearRequest {
    year: number;
    payroll?: Payroll;
}

/**
 * 
 */
export class PayrollApi extends runtime.BaseAPI {

    /**
     * Delete an existing payroll
     * Delete payroll
     */
    async deleteV1PayrollsYearRaw(requestParameters: DeleteV1PayrollsYearRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<void>> {
        if (requestParameters.year === null || requestParameters.year === undefined) {
            throw new runtime.RequiredError('year','Required parameter requestParameters.year was null or undefined when calling deleteV1PayrollsYear.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        if (this.configuration && this.configuration.accessToken) {
            // oauth required
            headerParameters["Authorization"] = await this.configuration.accessToken("production", ["kirin:user"]);
        }

        if (this.configuration && this.configuration.accessToken) {
            // oauth required
            headerParameters["Authorization"] = await this.configuration.accessToken("test", ["kirin:user"]);
        }

        const response = await this.request({
            path: `/api/v1/payrolls/{year}`.replace(`{${"year"}}`, encodeURIComponent(String(requestParameters.year))),
            method: 'DELETE',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.VoidApiResponse(response);
    }

    /**
     * Delete an existing payroll
     * Delete payroll
     */
    async deleteV1PayrollsYear(requestParameters: DeleteV1PayrollsYearRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<void> {
        await this.deleteV1PayrollsYearRaw(requestParameters, initOverrides);
    }

    /**
     * Returns the list of all years that have been configured ordered from the oldest to newest
     * List of years
     */
    async getV1PayrollsRaw(initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Array<Year>>> {
        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        if (this.configuration && this.configuration.accessToken) {
            // oauth required
            headerParameters["Authorization"] = await this.configuration.accessToken("production", ["kirin:user"]);
        }

        if (this.configuration && this.configuration.accessToken) {
            // oauth required
            headerParameters["Authorization"] = await this.configuration.accessToken("test", ["kirin:user"]);
        }

        const response = await this.request({
            path: `/api/v1/payrolls`,
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => jsonValue.map(YearFromJSON));
    }

    /**
     * Returns the list of all years that have been configured ordered from the oldest to newest
     * List of years
     */
    async getV1Payrolls(initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Array<Year>> {
        const response = await this.getV1PayrollsRaw(initOverrides);
        return await response.value();
    }

    /**
     * Retrieve the payroll for the selected year
     * Retrieve payroll
     */
    async getV1PayrollsYearRaw(requestParameters: GetV1PayrollsYearRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Payroll>> {
        if (requestParameters.year === null || requestParameters.year === undefined) {
            throw new runtime.RequiredError('year','Required parameter requestParameters.year was null or undefined when calling getV1PayrollsYear.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        if (this.configuration && this.configuration.accessToken) {
            // oauth required
            headerParameters["Authorization"] = await this.configuration.accessToken("production", ["kirin:user"]);
        }

        if (this.configuration && this.configuration.accessToken) {
            // oauth required
            headerParameters["Authorization"] = await this.configuration.accessToken("test", ["kirin:user"]);
        }

        const response = await this.request({
            path: `/api/v1/payrolls/{year}`.replace(`{${"year"}}`, encodeURIComponent(String(requestParameters.year))),
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => PayrollFromJSON(jsonValue));
    }

    /**
     * Retrieve the payroll for the selected year
     * Retrieve payroll
     */
    async getV1PayrollsYear(requestParameters: GetV1PayrollsYearRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Payroll> {
        const response = await this.getV1PayrollsYearRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * Create a new payroll if it does not exist
     * Create payroll
     */
    async postV1PayrollsYearRaw(requestParameters: PostV1PayrollsYearRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<void>> {
        if (requestParameters.year === null || requestParameters.year === undefined) {
            throw new runtime.RequiredError('year','Required parameter requestParameters.year was null or undefined when calling postV1PayrollsYear.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        headerParameters['Content-Type'] = 'application/json';

        if (this.configuration && this.configuration.accessToken) {
            // oauth required
            headerParameters["Authorization"] = await this.configuration.accessToken("production", ["kirin:user"]);
        }

        if (this.configuration && this.configuration.accessToken) {
            // oauth required
            headerParameters["Authorization"] = await this.configuration.accessToken("test", ["kirin:user"]);
        }

        const response = await this.request({
            path: `/api/v1/payrolls/{year}`.replace(`{${"year"}}`, encodeURIComponent(String(requestParameters.year))),
            method: 'POST',
            headers: headerParameters,
            query: queryParameters,
            body: PayrollToJSON(requestParameters.payroll),
        }, initOverrides);

        return new runtime.VoidApiResponse(response);
    }

    /**
     * Create a new payroll if it does not exist
     * Create payroll
     */
    async postV1PayrollsYear(requestParameters: PostV1PayrollsYearRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<void> {
        await this.postV1PayrollsYearRaw(requestParameters, initOverrides);
    }

    /**
     * Update an existing payroll
     * Update payroll
     */
    async putV1PayrollsYearRaw(requestParameters: PutV1PayrollsYearRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<void>> {
        if (requestParameters.year === null || requestParameters.year === undefined) {
            throw new runtime.RequiredError('year','Required parameter requestParameters.year was null or undefined when calling putV1PayrollsYear.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        headerParameters['Content-Type'] = 'application/json';

        if (this.configuration && this.configuration.accessToken) {
            // oauth required
            headerParameters["Authorization"] = await this.configuration.accessToken("production", ["kirin:user"]);
        }

        if (this.configuration && this.configuration.accessToken) {
            // oauth required
            headerParameters["Authorization"] = await this.configuration.accessToken("test", ["kirin:user"]);
        }

        const response = await this.request({
            path: `/api/v1/payrolls/{year}`.replace(`{${"year"}}`, encodeURIComponent(String(requestParameters.year))),
            method: 'PUT',
            headers: headerParameters,
            query: queryParameters,
            body: PayrollToJSON(requestParameters.payroll),
        }, initOverrides);

        return new runtime.VoidApiResponse(response);
    }

    /**
     * Update an existing payroll
     * Update payroll
     */
    async putV1PayrollsYear(requestParameters: PutV1PayrollsYearRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<void> {
        await this.putV1PayrollsYearRaw(requestParameters, initOverrides);
    }

}
