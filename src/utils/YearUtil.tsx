import { OptionProps } from "antd/es/select";
import { Year } from "../kirin";

export class YearUtil {
    static sort(years: Year[]): Year[] {
        return years.sort((a: Year, b: Year) => (b?.year ?? 0) - (a?.year ?? 0));
    }

    static transform(reqYears: Year[]): OptionProps[] {
        const sortedYears = YearUtil.sort(reqYears);
        const locyears: OptionProps[] = sortedYears.map((year: Year) => ({
            label: year.year,
            value: year.year,
            children: null,
        }));
        return locyears;
    }

    static getCurrentYear(debug: boolean, years: OptionProps[]): number {
        const localYear = localStorage.getItem('selectedYear');
        if(debug) console.log("localYear",localYear);

        const parsedSelectedYear = localYear ? parseInt(localYear) : 0;
        if(debug) console.log("parsedSelectedYear", parsedSelectedYear);
        
        const yearExists = years.some((year) => year.value === parsedSelectedYear);
        if(debug) console.log("yearExists", yearExists);
        
        if(yearExists) return parsedSelectedYear;
        else return years[0]?.value;
    }
}