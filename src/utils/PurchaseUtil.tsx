import { Purchase } from '../kirin';

export class PurchaseUtil {
    static sumPurchases(purchases: Purchase[]): number {
        let total = 0;
        purchases.forEach(purchase => {
            total += purchase.price;
        });
        return total;
    }
}