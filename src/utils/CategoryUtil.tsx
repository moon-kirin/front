import { Tooltip } from 'antd';
import * as icons from '@ant-design/icons';
import { Category } from '../kirin';

export function CategoryUtil() {
    function render(categoryId: number, categories: Category[]): JSX.Element {
        const category = categories.find(cat => cat.id === categoryId);

        if (category?.icon) {
            const IconComponent = icons[category.icon];
            return (
                <Tooltip title={category.description}>
                    <IconComponent /> {category.name}
                </Tooltip>
            );
        }
        if (category) {
            return (
                <Tooltip title={category.description}>
                    {category.name}
                </Tooltip>
            );
        }
        return <span>{categoryId}</span>;
    }

    return { render };
}