import { ItemType } from "antd/es/breadcrumb/Breadcrumb";
import { HomeOutlined } from '@ant-design/icons';
import { Link } from "react-router-dom";

export class BreadcrumbUtil {
    static get(path: string, name: string): ItemType[] {
        return [
            { 
                title: <Link to='/'><HomeOutlined /></Link> 
            },
            { 
                title: <Link to={`/${path}`}>{name}</Link> 
            }
        ];
    }
}