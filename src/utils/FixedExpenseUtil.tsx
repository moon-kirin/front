import { FixedExpense, FixedExpenseTypeEnum } from '../kirin';

export class FixedExpenseUtil {
    static sumFixedExpenses(fixedExpenses: FixedExpense[]): number {
        let total = 0;
        fixedExpenses.forEach(fixedExpense => {
            total += fixedExpense.type === FixedExpenseTypeEnum.Monthly ? fixedExpense.price * fixedExpense.months.length
                 : fixedExpense.price;
        });
        return total;
    }
}