import { Route, Routes } from "react-router-dom";
import AppError from "../components/error/AppError";
import Forbidden from "../components/error/Forbidden";
import Loading from "../components/helpers/Loading";
import HomeSection from "../components/sections/HomeSection";
import PayrollSection from "../components/sections/PayrollSection";
import FixedExpensesSection from "../components/sections/FixedExpensesSection";
import PurchaseSection from "../components/sections/PurchaseSection";
import DashboardSection from "../components/sections/DashboardSection";

export class RouteUtil {
    static getRoutes(status: string): JSX.Element {
        let content;
        switch (status) {
            case 'initial':
                content = <Loading />;
                break;
            case 'forbidden':
                content = <Forbidden />;
                break;
            case 'empty':
                content = <HomeSection />;
                break;
            case 'ok':
                content = (
                    <Routes>
                        <Route path="/" element={<HomeSection />} />
                        <Route path="/payroll" element={<PayrollSection />} />
                        <Route path="/fixed-expenses" element={<FixedExpensesSection />} />
                        <Route path="/purchase" element={<PurchaseSection />} />
                        <Route path="/dashboard" element={<DashboardSection />} />
                    </Routes>
                );
                break;
            default: 
                content = <AppError />;
        }
        return content;
    }
}