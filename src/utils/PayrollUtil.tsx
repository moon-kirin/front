import { Budget, Payroll, Saving } from "../kirin";

export class PayrollUtil {
    static sumMonths(payroll: Payroll): number {
        return (Number(payroll.payroll01) || 0) +
            (Number(payroll.payroll02) || 0) +
            (Number(payroll.payroll03) || 0) +
            (Number(payroll.payroll04) || 0) +
            (Number(payroll.payroll05) || 0) +
            (Number(payroll.payroll06) || 0) +
            (Number(payroll.payroll07) || 0) +
            (Number(payroll.payroll08) || 0) +
            (Number(payroll.payroll09) || 0) +
            (Number(payroll.payroll10) || 0) +
            (Number(payroll.payroll11) || 0) +
            (Number(payroll.payroll12) || 0);
    }

    static sumExtra(payroll: Payroll): number {
        const extra = payroll.extra?.reduce((acc, curr) => Number(acc) + Number(curr.payroll), 0);
        if(extra === undefined) return 0;
        else return extra;
    }

    static sumBudgets(budgets: Budget[]): number {
        let sum = 0;
        budgets.forEach(budget => {
            if (budget.type === "MONTHLY") {
                sum += budget.budget * 12;
            } else {
                sum += budget.budget;
            }
        });
        return sum;
    }

    static sumSavings(savings: Saving[]): number {
        let sum = 0;
        savings.forEach(saving => {
            sum += saving.amount;
        });
        return sum;
    }
}