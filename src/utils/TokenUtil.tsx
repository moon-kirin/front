import { TTokenData } from "react-oauth2-code-pkce/dist/types";

export class TokenUtil {
    static getUsername(tokenData: TTokenData): string {
        if (tokenData.name && tokenData.name.trim() !== '') {
            return tokenData.name;
        } else {
            return tokenData.preferred_username;
        }
    }

    static getInitials(name: string): string {
        const separator = /[ .]/;
        return name
            .split(separator)
            .map((name) => name.charAt(0))
            .join('');
    }
}