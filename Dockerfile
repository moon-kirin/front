FROM nginx:stable-alpine-slim

COPY nginx-custom.conf /etc/nginx/conf.d/default.conf
COPY env.sh /docker-entrypoint.d/env.sh
RUN chmod +x /docker-entrypoint.d/env.sh 
COPY dist /usr/share/nginx/html
